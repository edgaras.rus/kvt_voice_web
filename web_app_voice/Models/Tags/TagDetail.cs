﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Tags
{
    [Table("TagDetails", Schema = "tags")]
    public class TagDetail
    {
        [Key]
        public int TagDetailID { get; set; }

        public int TagID { get; set; }

        public int? OrganizationScannerID { get; set; }

        public int RSSI { get; set; }

        public DateTime RecordDate { get; set; }

        [ForeignKey("TagID")]
        public virtual Tag Tag { get; set; } 

    }
}
