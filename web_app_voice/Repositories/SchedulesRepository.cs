﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Data;
using web_app_voice.Models.Transports;
using web_app_voice.Repositories.Interfaces;

namespace web_app_voice.Repositories
{
    public class SchedulesRepository : ISchedulesRepository
    {
        private readonly ApplicationDbContext _context;

        public SchedulesRepository(ApplicationDbContext applicationDbContext)
        {
            _context = applicationDbContext;
        }

        public DailySchedule LoadDailyScheduleByOrganizationAndDeclare(int organizationID, DateTime declareDate)
        {
            return _context.DailySchedules
                    .Where(x => x.OrganizationID == organizationID && x.DeclareDate == declareDate).OrderByDescending(x => x.UpdateDate)
                    .FirstOrDefault();
        }


        public DailySchedule LoadLastOrganizationDailySchedule(int organizationID)
        {
            return _context.DailySchedules
                .Where(x => x.OrganizationID == organizationID)
                .Include(tt => tt.TransportTrips)
                .OrderByDescending(x => x.UpdateDate).FirstOrDefault();
        }

  


        public List<TransportTrip> TransportTripsBySchedule(int dailyScheduleID)
        {
            return _context.TransportTrips
                .Where(x => x.DailyTransportTripID == dailyScheduleID)
                .ToList();
        }

        public List<DailySchedule> LoadDailySchedulesByDeclare(DateTime declareDate)
        {

            List<DailySchedule> result = new List<DailySchedule>();

            var organizations = _context.Organizations.ToList();

            foreach(var organization in organizations)
            {
                result.Add(_context.DailySchedules
                    .Where(ds => ds.OrganizationID == organization.OrganizationID && ds.DeclareDate == declareDate)
                    .Include(tt => tt.TransportTrips)
                    .FirstOrDefault());
            }

            return result;
        }


        public void SaveDailySchedule(DailySchedule dailySchedule)
        {
            if(dailySchedule.DailyTransportTripID > 0)
            {
                _context.DailySchedules.Update(dailySchedule);
                _context.SaveChanges();
            }
            else
            {
                _context.DailySchedules.Add(dailySchedule);
                _context.SaveChanges();
            }
        }


        #region Station

        //---------------------------------------------------------------------------------------------------------------------/

        public List<Station> LoadStations()
        {
           return _context.Stations.ToList();
        }

        public void SaveStations(List<Station> stations)
        {
            _context.Stations.AddRange(stations);
            _context.SaveChanges();
        }

        public void UpdateStations(List<Station> stations)
        {
            _context.Stations.UpdateRange(stations);
            _context.SaveChanges();
        }


        //---------------------------------------------------------------------------------------------------------------------/

        #endregion
    }
}
