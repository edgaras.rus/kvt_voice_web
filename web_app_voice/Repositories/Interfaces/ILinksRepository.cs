﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Models.Tags;
using web_app_voice.ModelsDto;

namespace web_app_voice.Repositories.Interfaces
{
    public interface ILinksRepository
    {
        List<TagLinkDto> LoadTagLinks(int? organizationID);

        List<TagLinkDto> LoadTagLinksByType(int? organizationID, int tagLinkTypeID);

        TagLinkCreateEditDto LoadTagLinkCreateEditDto(int tagLinkID);

        void SaveUpdate(TagLinkCreateEditDto tagLinkCreateEditDto);

        bool IsLinkWithClientSystemIDExists(string clientSystemID, int linkTypeID, int organizationID, int? tagLinkID);

        bool IsLinkWithTagIDExists(string tagID, int linkTypeID, int organizationID, int? tagLinkID);

        TransportLinkDetailsDto GetTransportLinkDetailsDto(int tagLinkID);

        void Remove(int id);

        List<TagLink> GetTagLinks();

        void UpdateTagLink(TagLink tagLink);

    }
}
