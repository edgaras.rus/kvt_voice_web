﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Audio
{
    [Table("AudioTypes", Schema = "audios")]
    public class AudioType
    {
        public int AudioTypeID { get; set; }

        public string Name { get; set; }
    }
}
