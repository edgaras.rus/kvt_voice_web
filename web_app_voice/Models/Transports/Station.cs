﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Transports
{
    [Table("Stations", Schema = "transports")]
    public class Station
    {
        [Key]
        public int StationID { get; set; }

        [Required]
        public int OrganizationID { get; set; }

        [Required]
        public string ClientSystemID { get; set; }

        [Required]
        public string StationName { get; set; }

        public string StationDirection { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public string PlayList { get; set; }

        public string ShortPlayList { get; set; }

        public string ContentLithunian { get; set; }

        public string ShortContentLithunian { get; set; }

        public string ContentEnglish { get; set; }

        public string ShortContentEnglish { get; set; }

    }
}
