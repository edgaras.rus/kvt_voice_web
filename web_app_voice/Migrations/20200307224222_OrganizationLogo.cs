﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace web_app_voice.Migrations
{
    public partial class OrganizationLogo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OrganizationLogo",
                schema: "organizations",
                table: "Organizations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrganizationLogo",
                schema: "organizations",
                table: "Organizations");
        }
    }
}
