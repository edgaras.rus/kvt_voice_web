﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.AudiosDto
{
    public class AudioCreateEditDto
    {
        public int AudioID { get; set; }

        public int OrganizationID { get; set; }

        public string OrganizationName { get; set; }

        public int AudioTypeID { get; set; }

        public string AudioTypeName { get; set; }

        public string SystemName { get; set; }

        public string SpeakOutLTU { get; set; }

        public string SpeakOutEN { get; set; }

        public bool Active { get; set; }

        public byte[] Bytes { get; set; }

        public string FileBase64 { get; set; }


        public List<SelectListItem> Organizations { get; set; }

        public List<SelectListItem> AudioTypes { get; set; }


        public AudioCreateEditDto()
        {
            this.Organizations = new List<SelectListItem>();
            this.AudioTypes = new List<SelectListItem>()
            {
                new SelectListItem
                {
                    Text = "System",
                    Value = "1"

                },

                new SelectListItem
                {
                    Text ="Transport",
                    Value = "2"
                },

                 new SelectListItem
                {
                    Text ="Station",
                    Value = "3"
                },

                new SelectListItem
                {
                     Text = "Direction",
                    Value = "4"
                },
            };
        }
    }
}
