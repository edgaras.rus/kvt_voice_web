﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using web_app_voice.Models;
using web_app_voice.Repositories.Interfaces;

namespace web_app_voice.Controllers
{
    [Authorize]
    public class DashBoardController : Controller
    {

        private readonly IDashBoardRepository _dashBoardRepository;
        private readonly UserManager<User> _userManager;

        public DashBoardController(IDashBoardRepository dashBoardRepository, UserManager<User> userManager)
        {
            _dashBoardRepository = dashBoardRepository;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            var data = _dashBoardRepository.LoadDashBoard(GetUser().OrganizationID);
            return View(data);
        }


        private User GetUser()
        {
            return _userManager.FindByIdAsync(User.FindFirstValue(ClaimTypes.NameIdentifier)).Result;
        }
    }
}
