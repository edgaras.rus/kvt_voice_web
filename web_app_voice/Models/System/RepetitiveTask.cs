﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.System
{

    [Table("RepetitiveTasks", Schema = "systems")]
    public class RepetitiveTask
    {
        [Key]
        public int RepetitiveTaskID { get; set; }

        public int RepetitiveTaskTypeID { get; set; }

        public string Name { get; set; }

        public int Frequency { get; set; }

        public bool Enabled { get; set; }

        public long ElapsedMilliseconds { get; set; }

        public DateTime? LastExecuteTime { get; set; }

        public DateTime? NexExecuteTime { get; set; }

    }
}
