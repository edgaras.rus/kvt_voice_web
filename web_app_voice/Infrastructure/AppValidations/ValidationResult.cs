﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Extensions;

namespace web_app_voice.Infrastructure.AppValidations
{
    public class ValidationResult
    {
        public bool HasError
        {
            get
            {
                return Errors.Count > 0;
            }
        }
        public List<PropertyError> Errors { get; private set; }

        public ValidationResult()
        {
            Errors = new List<PropertyError>();
        }

        public ValidationResult(string errorKeyword)
        {
            Errors = new List<PropertyError>();

            if (!errorKeyword.StringIsEmpty())
                Errors.Add(new PropertyError(errorKeyword));
        }

        public ValidationResult(PropertyError error)
        {
            Errors = new List<PropertyError>();

            if (error != null)
                AppendError(error);
        }

        public ValidationResult(List<PropertyError> errors)
        {
            if (errors != null)
                Errors = errors;
            else
                Errors = new List<PropertyError>();
        }

        public void AppendError(string errorKeyword)
        {
            if (!errorKeyword.StringIsEmpty())
                Errors.Add(new PropertyError(errorKeyword));
        }

        public void AppendError(PropertyError error)
        {
            if (error != null)
                Errors.Add(error);
        }

        public void AppendError(string fieldName, string errorMessage)
        {
            if (!errorMessage.StringIsEmpty())
                Errors.Add(new PropertyError(fieldName, errorMessage));
        }

        public JsonResult JsonResult()
        {
            return new JsonResult(new { errors = Errors, hasError = HasError });
        }
    }
}