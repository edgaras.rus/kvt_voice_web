﻿"use strict";

var audioFilesValues = function (options) {

    var vars = {
        table: {},
        AudioType: 0,
        AudioFiles: 0
    }

    var root = this;

    this.construct = function (options) {
        $.extend(vars, options);

        initDataTable();

        var placeholderElement = $('#modal-placeholder');

        $('#dataTable tbody').on('click', 'button', function () {
            var data = vars.table.row($(this).parents('tr')).data();

            $.ajax({
                url: vars.createEditModalUrl,
                type: "POST",
                dataType: "text",
                data: {
                    audioID: data.audioID,
                },
                error: function (response) {
                    if (!response.Success) {
                        alert("Server error");
                    }
                },

                success: function (response) {
                    placeholderElement.html(response);
                    placeholderElement.find('.modal').modal('show');
                }
            });
        });

        placeholderElement.on('click', '[data-save="modal"]', function (event) {
            event.preventDefault();

            var form = $(this).parents('.modal').find('form');
            var actionUrl = form.attr('action');
            var dataToSend = form.serialize();



            const file = document.querySelector('input[type=file]').files[0];
            const reader = new FileReader();



            if (file) {
                reader.readAsDataURL(file);
                reader.onloadend = function () {
                    var base64data = reader.result;
                    dataToSend = dataToSend + "&FileBase64=" + base64data.split(',')[1];

                    $.post(actionUrl, dataToSend).done(function (data) {
                        var newBody = $('.modal-body', data);
                        placeholderElement.find('.modal-body').replaceWith(newBody);

                        var isValid = newBody.find('[name="IsValid"]').val() == 'True';
                        if (isValid) {
                            placeholderElement.find('.modal').modal('hide');
                            vars.table.ajax.reload();
                        }
                    });

                }
            }
            else {
                $.post(actionUrl, dataToSend).done(function (data) {
                    var newBody = $('.modal-body', data);
                    placeholderElement.find('.modal-body').replaceWith(newBody);

                    var isValid = newBody.find('[name="IsValid"]').val() == 'True';
                    if (isValid) {
                        placeholderElement.find('.modal').modal('hide');
                        vars.table.ajax.reload();
                    }
                });
            }
        });
    }

    function reloadTable() {
        vars.table.ajax.reload();
    }

    function initDataTable() {
        vars.table = $("#dataTable").DataTable({

            dom: '<"html5buttons"B>lTfgitp',
            // Design Assets
            stateSave: true,
            autoWidth: true,
            // ServerSide Setups
            processing: true,
            serverSide: true,
            // Paging Setups
            paging: true,
            // Searching Setups
            searching: { regex: true },
            // Ajax


            ajax: {
                url: vars.getTableDataUrl,
                type: "POST",
                dataType: "json",
                data: function (d) {
                    var object = {
                        dtParameters: d,
                        organizationID: vars.organizationID,
                        deviceID: vars.deviceID,
                        audioType: vars.AudioType,
                        audioFiles: vars.AudioFiles
                    }
                    return object;
                }
            },
            buttons: [
                {
                    extend: 'collection',
                    text: 'Audio Type',
                    buttons: [
                        { text: 'ALL', action: function () { vars.AudioType = 0; reloadTable() } },
                        { text: 'Transport', action: function () { vars.AudioType = 2; reloadTable()  } },
                        { text: 'Station', action: function () { vars.AudioType = 3; reloadTable() } },
                        { text: 'Direction', action: function () { vars.AudioType = 4; reloadTable() } },
                        { text: 'System', action: function () { vars.AudioType = 1; reloadTable() } }
                    ],
                    fade: true
                },                                                                                                                            
                {
                    extend: 'collection',
                    text: 'Audio Files',
                    buttons: [
                        { text: 'ALL', action: function () { vars.AudioFiles = 0; reloadTable()} },
                        { text: 'With File', action: function () { vars.AudioFiles = 1; reloadTable()} },
                        { text: 'Without File', action: function () { vars.AudioFiles = 2; reloadTable()} },
                      
                    ],
                    fade: true
                },

                { extend: 'copy' },
                { extend: 'csv' },
                { extend: 'excel', title: 'ExampleFile' },
                { extend: 'pdf', title: 'ExampleFile' },

                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }],

            columns: [
                { "width": "10%", data: "systemName", "name": "Name" },
                { "width": "10%", data: "audioTypeName", "name": "File Type" },
                { "width": "10%", data: "active", "name": "Status" },
                { "width": "10%", data: "speakOutLTU", "name": "LTU Text" },
                { "width": "10%", data: "speakOutEN", "name": "ENG Text" },
                { "width": "10%", data: "updateDate", "name": "Last Update Date" },
                {
                    data: undefined, width: "25%", "render": function (data, type, full, meta) {
                        if (full.bytes !== null)
                            return '<audio controls preload=\'none\'> <source src=\'data:audio/mp3; base64,' + full.audioBase64 + '\' type = "audio/mp3" /> </audio>';
                        return "";
                    }
                },
                {
                    data: undefined, width: "5%", "render": function (data, type, full, meta) {
                        return '<button data-toggle="ajax-modal" data-target="#add-contact" class="btn btn-info dataEdit">Edit</button>';
                    }

                }
            ],
            columnDefs: [
                { targets: [0, 1, 2, 3, 4, 5], orderable: true },
                { targets: [6, 7], orderable: false },

                { targets: "no-sort", orderable: false },
                { targets: "no-search", searchable: false },
                {
                    targets: "trim",
                    render: function (data, type, full, meta) {
                        if (type === "display") {
                            data = strtrunc(data, 10);
                        }
                        return data;
                    }
                }
            ]

        });
    }

    this.construct(options);
}

