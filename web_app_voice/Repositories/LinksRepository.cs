﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Data;
using web_app_voice.Models.Tags;
using web_app_voice.Models.Transports;
using web_app_voice.ModelsDto;
using web_app_voice.Repositories.Interfaces;

namespace web_app_voice.Repositories
{
    public class LinksRepository : ILinksRepository
    {
        private readonly ApplicationDbContext _context;

        public LinksRepository(ApplicationDbContext applicationDbContext)
        {
            _context = applicationDbContext;
        }


        public List<TagLink> GetTagLinks()
        {
            return _context.TagLinks.ToList();
        }


        public void UpdateTagLink(TagLink tagLink)
        {
            _context.TagLinks.Update(tagLink);
            _context.SaveChanges();
        }

        public List<TagLinkDto> LoadTagLinks(int? organizationID)
        {
            List<TagLinkDto> results = new List<TagLinkDto>();

            if (organizationID.HasValue)
            {
                results = _context.TagLinks.Where(tl => tl.OrganizationID == organizationID)
                     .Include(tl => tl.Organization)
                     .Include(tl => tl.TagLinkType)

                 .Select(tl => new TagLinkDto
                 {
                     TagLinkID = tl.TagLinkID,
                     OrganizationID = tl.OrganizationID,
                     OrganizationName = tl.Organization.Name,
                     TagLinkTypeID = tl.TagLinkTypeID,
                     TagLinkTypeName = tl.TagLinkType.Name,
                     TagID = tl.TagID,
                     ClientSystemID = tl.ClientSystemID,
                     LastSeenDateTime = tl.LastSeenDate,
                     LastSeenDate = tl.LastSeenDate.HasValue ? tl.LastSeenDate.Value.ToString("yyyy-MM-dd HH:mm") : "",
                     LastPlanedDateTime = tl.LastPlanedDate,
                     LastPlanedDate = tl.LastPlanedDate.HasValue ? tl.LastPlanedDate.Value.ToString("yyyy-MM-dd HH:mm") : "",
                 })
                 .ToList();
            }
            else
            {
                results = _context.TagLinks
                  .Include(tl => tl.Organization)
                  .Include(tl => tl.TagLinkType)
                  .Select(tl => new TagLinkDto
                  {
                      TagLinkID = tl.TagLinkID,
                      OrganizationID = tl.OrganizationID,
                      OrganizationName = tl.Organization.Name,
                      TagLinkTypeID = tl.TagLinkTypeID,
                      TagLinkTypeName = tl.TagLinkType.Name,
                      TagID = tl.TagID,
                      ClientSystemID = tl.ClientSystemID,
                      LastSeenDateTime = tl.LastSeenDate,
                      LastSeenDate = tl.LastSeenDate.HasValue ? tl.LastSeenDate.Value.ToString("yyyy-MM-dd HH:mm") : "",
                      LastPlanedDateTime = tl.LastPlanedDate,
                      LastPlanedDate = tl.LastPlanedDate.HasValue ? tl.LastPlanedDate.Value.ToString("yyyy-MM-dd HH:mm") : "",
                  })
                  .ToList();
            }

            return results;
        }

        public List<TagLinkDto> LoadTagLinksByType(int? organizationID, int tagLinkTypeID)
        {
            List<TagLinkDto> results = new List<TagLinkDto>();

            if (organizationID.HasValue)
            {
                results = _context.TagLinks.Where(tl => tl.OrganizationID == organizationID && tl.TagLinkTypeID == tagLinkTypeID)
                     .Include(tl => tl.Organization)
                     .Include(tl => tl.TagLinkType)
                     .Select(tl => new TagLinkDto
                     {
                         TagLinkID = tl.TagLinkID,
                         OrganizationID = tl.OrganizationID,
                         OrganizationName = tl.Organization.Name,
                         TagLinkTypeID = tl.TagLinkTypeID,
                         TagLinkTypeName = tl.TagLinkType.Name,
                         TagID = tl.TagID,
                         ClientSystemID = tl.ClientSystemID,
                         LastSeenDateTime = tl.LastSeenDate,
                         LastSeenDate = tl.LastSeenDate.HasValue ? tl.LastSeenDate.Value.ToString("yyyy-MM-dd HH:mm") : "",
                         LastPlanedDateTime = tl.LastPlanedDate,
                         LastPlanedDate = tl.LastPlanedDate.HasValue ? tl.LastPlanedDate.Value.ToString("yyyy-MM-dd HH:mm") : "",
                     }).ToList();
            }
            else
            {
                results = _context.TagLinks.Where(tl => tl.TagLinkTypeID == tagLinkTypeID)
                  .Include(tl => tl.Organization)
                  .Include(tl => tl.TagLinkType)
                  .Select(tl => new TagLinkDto
                  {
                      TagLinkID = tl.TagLinkID,
                      OrganizationID = tl.OrganizationID,
                      OrganizationName = tl.Organization.Name,
                      TagLinkTypeID = tl.TagLinkTypeID,
                      TagLinkTypeName = tl.TagLinkType.Name,
                      TagID = tl.TagID,
                      ClientSystemID = tl.ClientSystemID,
                      LastSeenDateTime = tl.LastSeenDate,
                      LastSeenDate = tl.LastSeenDate.HasValue ? tl.LastSeenDate.Value.ToString("yyyy-MM-dd HH:mm") : "",
                      LastPlanedDateTime = tl.LastPlanedDate,
                      LastPlanedDate = tl.LastPlanedDate.HasValue ? tl.LastPlanedDate.Value.ToString("yyyy-MM-dd HH:mm") : "",

                  })
                  .ToList();
            }

            if (tagLinkTypeID == 3)
            {

                var stations = _context.Stations.ToList();

                foreach (var result in results)
                {
                    var value = stations
                         .Where(x => x.OrganizationID == result.OrganizationID && x.ClientSystemID == result.ClientSystemID)
                         .FirstOrDefault();

                    if (value != null)
                    {
                        result.ClientSystemName = value.StationName;
                    }

                }
            }

            return results;
        }

        public TagLinkCreateEditDto LoadTagLinkCreateEditDto(int tagLinkID)
        {
            return _context.TagLinks.Where(tl => tl.TagLinkID == tagLinkID).Include(tl => tl.TagLinkType).Include(tl => tl.Organization).Select(tl => new TagLinkCreateEditDto
            {
                TagLinkID = tl.TagLinkID,
                OrganizationID = tl.OrganizationID,
                OrganizationName = tl.Organization.Name,
                TagLinkTypeID = tl.TagLinkTypeID,
                TagLinkTypeName = tl.TagLinkType.Name,
                TagID = tl.TagID,
                ClientSystemID = tl.ClientSystemID

            }).FirstOrDefault();
        }


        public void SaveUpdate(TagLinkCreateEditDto tagLinkCreateEditDto)
        {
            if (tagLinkCreateEditDto.TagLinkID > 0)
            {
                var tagLink = _context.TagLinks.Where(x => x.TagLinkID == tagLinkCreateEditDto.TagLinkID).FirstOrDefault();

                tagLink.OrganizationID = tagLinkCreateEditDto.OrganizationID;
                tagLink.TagLinkTypeID = tagLinkCreateEditDto.TagLinkTypeID;
                tagLink.TagID = tagLinkCreateEditDto.TagID;
                tagLink.ClientSystemID = tagLinkCreateEditDto.ClientSystemID;

                _context.TagLinks.Update(tagLink);
                _context.SaveChanges();

            }
            else
            {
                TagLink _tagLink = new TagLink()
                {
                    OrganizationID = tagLinkCreateEditDto.OrganizationID,
                    TagLinkTypeID = tagLinkCreateEditDto.TagLinkTypeID,
                    TagID = tagLinkCreateEditDto.TagID,
                    ClientSystemID = tagLinkCreateEditDto.ClientSystemID
                };

                _context.TagLinks.Add(_tagLink);
                _context.SaveChanges();
            }
        }

        public void Remove(int id)
        {
            var tagLink = _context.TagLinks.Where(x => x.TagLinkID == id).FirstOrDefault();

            _context.TagLinks.Remove(tagLink);
            _context.SaveChanges();
        }

        public bool IsLinkWithClientSystemIDExists(string clientSystemID, int linkTypeID, int organizationID, int? tagLinkID)
        {
            if (!tagLinkID.HasValue)
            {
                if (_context.TagLinks.Any(tl => tl.ClientSystemID == clientSystemID && tl.TagLinkTypeID == linkTypeID && tl.OrganizationID == organizationID))
                    return true;
            }
            else
            {
                if (_context.TagLinks.Any(tl => tl.ClientSystemID == clientSystemID && tl.TagLinkTypeID == linkTypeID
                    && tl.OrganizationID == organizationID && tl.TagLinkID != tagLinkID))
                    return true;
            }

            return false;
        }

        public bool IsLinkWithTagIDExists(string tagID, int linkTypeID, int organizationID, int? tagLinkID)
        {
            int _tagID = int.Parse(tagID);

            if (!tagLinkID.HasValue)
            {
                if (_context.TagLinks.Any(tl => tl.TagID == _tagID && tl.TagLinkTypeID == linkTypeID && tl.OrganizationID == organizationID))
                    return true;
            }
            else
            {
                if (_context.TagLinks.Any(tl => tl.TagID == _tagID && tl.TagLinkTypeID == linkTypeID
                    && tl.OrganizationID == organizationID && tl.TagLinkID != tagLinkID))
                    return true;
            }

            return false;
        }

        public TransportLinkDetailsDto GetTransportLinkDetailsDto(int tagLinkID)
        {
            TagLink tagLink = _context.TagLinks.Where(tl => tl.TagLinkID == tagLinkID).FirstOrDefault();

            if (tagLink == null)
                return new TransportLinkDetailsDto();

            DailySchedule dailySchedule = _context.DailySchedules
                .Where(x => x.OrganizationID == tagLink.OrganizationID)
                .OrderByDescending(ds => ds.UpdateDate)
                .Include(ds => ds.TransportTrips)
                .FirstOrDefault();

            if (dailySchedule == null)
                return new TransportLinkDetailsDto();


            var a = dailySchedule.TransportTrips.Where(tt => tt.ClientSystemID == tagLink.ClientSystemID).ToList();

            TransportLinkDetailsDto result = dailySchedule.TransportTrips
                .Where(tt => tt.ClientSystemID == tagLink.ClientSystemID && DateTime.Now >= tt.StartDate &&  DateTime.Now <= tt.EndDate)
                .Select(tt => new TransportLinkDetailsDto()
                {
                    TransportTypeID = 1,
                    RouteName = tt.RouteName,
                    DirectionName = tt.DirectionName,
                    FromDate = tt.StartDate.ToString("yyyy-MM-dd HH:mm:ss"),
                    ToDate = tt.EndDate.ToString("yyyy-MM-dd HH:mm:ss"),
                    PlayListLong = tt.PlayList,
                    PlayListShort = tt.ShortPlayList
                }).FirstOrDefault();

            if (result == null)
                result = new TransportLinkDetailsDto();

            return result;
        }
    }
}
