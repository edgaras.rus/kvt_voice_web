﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.TagsDto
{
    public class TagDetailDto
    {
        public int Id { get; set; }

        public bool IsActive { get; set; }

        public string Instance { get; set; }

        public string Namespace { get; set; }

        public string Mac { get; set; }

        public int Type { get; set; }

        public string TransportRoute { get; set; }

        public string TransportRouteNumber { get; set; }

        public string StationName { get; set; }

        public string StationId { get; set; }

        public DateTime LastSeenDate { get; set; }

        public DateTime ChartStartDate { get; set; }

        public DateTime ChartEndDate { get; set; }
        public TagChartDto TagChartDto { get; set; }

        public TagDetailDto()
        {
            this.Id = 9999;
            this.Instance = "88889999";
            this.Namespace = "122222222222222222222";
            this.Mac = "AA:BB:CC:DD:EE:FF";
            this.Type = 1;
            this.TransportRoute = "Į Geležinkelio Stoty";
            this.TransportRouteNumber = "13";
            this.StationName = "Mituvos";
            this.StationId = "439";
            this.LastSeenDate = DateTime.Now;
            this.ChartStartDate = DateTime.Now;
            this.ChartEndDate = DateTime.Now;
        }
    }
}
