﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Enums
{
    public enum TransportTypes
    {
        [Description("Autobusas")]
        Bus = 10,
        [Description("Troleibusas")]
        Trolley = 20,
        [Description("Stotelė")]
        Station = 30,
        [Description("Kita")]
        Other = 40,
    }
}
