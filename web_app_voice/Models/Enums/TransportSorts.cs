﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Enums
{
    public enum TransportSort : short
    {
        [Description("None")]
        None = 0,
        [Description("Name")]
        Name = 10,
        [Description("Transport")]
        Transport = 20,
        [Description("Status")]
        Status = 30,
    }
}
