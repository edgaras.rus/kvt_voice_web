﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.AndroidDto
{
    public class AudioAndroid
    {
        public int Id { get; set; }

        public int OrganizationId { get; set; }

        public string SystemName { get; set; }

        public string SpeakOutLithuania { get; set; }

        public string SpeakOutEnglish { get; set; }

        public string UpdateDate { get; set; }

        [DefaultValue(false)]
        public bool HasFile { get; set; }
    }
}
