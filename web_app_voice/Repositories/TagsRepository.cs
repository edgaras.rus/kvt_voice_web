﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Data;
using web_app_voice.Models.Enums;
using web_app_voice.Models.Organizations;
using web_app_voice.Models.Tags;
using web_app_voice.Models.Transports;
using web_app_voice.ModelsDto.AndroidDto;
using web_app_voice.ModelsDto.SensorDto;
using web_app_voice.ModelsDto.TagsDto;
using web_app_voice.Repositories.Interfaces;

namespace web_app_voice.Repositories
{
    public class TagsRepository : ITagsRepository
    {
        private readonly ApplicationDbContext _context;

        public TagsRepository(ApplicationDbContext applicationDbContext)
        {
            _context = applicationDbContext;
        }

        public List<Tag> GetTags() {

            return _context.Tags.ToList();
        }

        public List<TagDto> LoadTagListDto(TagsFilterDto filter, int? organizationID)
        {

            List<Tag> tags = new List<Tag>();

            if (organizationID == null)
            {
                tags = _context.Tags.ToList();
            }
            else
            {
                var organization = _context.Organizations.Where(o => o.OrganizationID == organizationID.Value).FirstOrDefault();
                tags = _context.Tags.Where(t => t.TagNamespace.ToUpper().Equals(organization.GUID)).ToList();
            }

            List<TagDto> list = new List<TagDto>();

            foreach (var tag in tags)
            {
                TagDto dtoTag = new TagDto
                {
                    Id = tag.TagID,
                    Mac = tag.TagMAC,
                    NameSpce = tag.TagNamespace,
                    Instance = tag.TagInstance,
                    Url = tag.TagUrl,
                    Type = tag.TagTypeID,
                    LastSeenDate = tag.LastSeenDate,
                    TagDetails = new List<TagDetailDto>(),
                    IsRegistered = tag.IsActive,
                };

                if (dtoTag.Instance.Length == 8)
                {
                    string type = dtoTag.Instance.Substring(0, 4);
                    switch (type)
                    {
                        case "9000":
                            dtoTag.TransportType = TransportTypes.Station;
                            break;
                        case "2000":
                            dtoTag.TransportType = TransportTypes.Trolley;
                            break;
                        case "1000":
                            dtoTag.TransportType = TransportTypes.Bus;
                            break;
                        default:
                            dtoTag.TransportType = TransportTypes.Other;
                            break;
                    }


                    if (dtoTag.LastSeenDate < DateTime.Now.AddDays(-7))
                    {
                        dtoTag.TransportState = TagStateTypes.Error;
                    }
                    else if (dtoTag.LastSeenDate < DateTime.Now.AddDays(-4))
                    {
                        dtoTag.TransportState = TagStateTypes.Warrning;
                    }
                    else
                    {
                        dtoTag.TransportState = TagStateTypes.Okey;
                    }
                    list.Add(dtoTag);
                }
            }

            if (filter.TransportTypes == null)
            {
                filter.TransportTypes = new List<int>();
            }

            if (!filter.TransportTypes.Any(x => x == (int)TransportTypes.Bus))
            {
                list = list.Where(x => x.TransportType != TransportTypes.Bus).ToList();
            }
            if (!filter.TransportTypes.Any(x => x == (int)TransportTypes.Trolley))
            {
                list = list.Where(x => x.TransportType != TransportTypes.Trolley).ToList();
            }
            if (!filter.TransportTypes.Any(x => x == (int)TransportTypes.Station))
            {
                list = list.Where(x => x.TransportType != TransportTypes.Station).ToList();
            }
            if (!filter.TransportTypes.Any(x => x == (int)TransportTypes.Other))
            {
                list = list.Where(x => x.TransportType != TransportTypes.Other).ToList();
            }

            if (filter.StateValues == null)
            {
                filter.StateValues = new List<int>();
            }
            if (!filter.StateValues.Any(x => x == (int)TagStateTypes.Okey))
            {
                list = list.Where(x => x.TransportState != TagStateTypes.Okey).ToList();
            }
            if (!filter.StateValues.Any(x => x == (int)TagStateTypes.Warrning))
            {
                list = list.Where(x => x.TransportState != TagStateTypes.Warrning).ToList();
            }
            if (!filter.StateValues.Any(x => x == (int)TagStateTypes.Error))
            {
                list = list.Where(x => x.TransportState != TagStateTypes.Error).ToList();
            }

            if (filter.Search == null)
            {
                filter.Search = String.Empty;
            }

            if (filter.Search != null && filter.Search.Length > 0)
            {
                list = list.Where(x => x.Instance.Contains(filter.Search)).ToList();
            }

            if (filter.SortType == (int)SortTypes.Asc)
            {
                if (filter.TransportSortType == (int)TransportSort.Name)
                {
                    list = list.OrderBy(x => x.Instance).ToList();
                }

                if (filter.TransportSortType == (int)TransportSort.Status)
                {
                    list = list.OrderBy(x => x.TransportState).ToList();
                }

                if (filter.TransportSortType == (int)TransportSort.Transport)
                {
                    list = list.OrderBy(x => x.TransportType).ToList();
                }
            }

            if (filter.SortType == (int)SortTypes.Desc)
            {
                if (filter.TransportSortType == (int)TransportSort.Name)
                {
                    list = list.OrderByDescending(x => x.Instance).ToList();
                }

                if (filter.TransportSortType == (int)TransportSort.Status)
                {
                    list = list.OrderByDescending(x => x.TransportState).ToList();
                }

                if (filter.TransportSortType == (int)TransportSort.Transport)
                {
                    list = list.OrderByDescending(x => x.TransportType).ToList();
                }
            }
            return list;
        }

        public TagDetailDto LoadTagDetailDto(int tagID, DateTime start, DateTime end)
        {
            TagDetailDto result = new TagDetailDto();

            var tag = _context.Tags.Where(t => t.TagID == tagID).Include(t => t.TagDetails).FirstOrDefault();

            if (tag != null)
            {
                result.Id = tag.TagID;
                result.IsActive = tag.IsActive;
                result.Mac = tag.TagMAC;
                result.Namespace = tag.TagNamespace;
                result.Instance = tag.TagInstance;
                result.Type = tag.TagTypeID;
                result.LastSeenDate = tag.LastSeenDate;

                result.ChartStartDate = start;
                result.ChartEndDate = end;

                result.TagChartDto = new TagChartDto();
                result.TagChartDto = LoadSmartTagDetails(result, tag);
            }

            return result;
        }

        private TagChartDto LoadSmartTagDetails(TagDetailDto tagDetailDto, Tag tag)
        {
            TagChartDto chartData = new TagChartDto();

            var selectedList = tag.TagDetails.Where(x => x.RecordDate >= tagDetailDto.ChartStartDate
                && x.RecordDate <= tagDetailDto.ChartEndDate).ToList();

            if (selectedList.Count > 0)
            {
                chartData = new TagChartDto()
                {
                    MaxRssi = selectedList.Max(x => x.RSSI),
                    MinRssi = selectedList.Min(x => x.RSSI),
                    AvgRssi = selectedList.Average(x => x.RSSI)
                };

                for (DateTime i = tagDetailDto.ChartStartDate; i < tagDetailDto.ChartEndDate.AddDays(1); i = i.AddDays(1))
                {
                    DateTime st_date = new DateTime(i.Year, i.Month, i.Day, 0, 0, 0);
                    DateTime end_date = new DateTime(i.Year, i.Month, i.Day, 23, 59, 59);

                    try
                    {
                        var _avg = selectedList
                            .Where(x => x.RecordDate >= st_date && x.RecordDate <= end_date)
                            .Average(y => y.RSSI);

                        chartData.ChartData.Add(new TagChartItemDto()
                        {
                            Time = st_date,
                            RSSI = _avg != null ? (int)_avg : 0
                        });


                    }
                    catch (Exception ex)
                    {

                    }

                }

            }
            else
            {
                chartData = new TagChartDto()
                {
                    MaxRssi = 0,
                    MinRssi = 0,
                    AvgRssi = 0,
                    ChartData = new List<TagChartItemDto>()
                };
            }

            return chartData;
        }

        public void SaveSensorData(List<SensorTag> datalist, string scannerGUID = null)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (scannerGUID == null)
                    {

                        foreach (SensorTag sensorTag in datalist)
                        {
                            


                            string _instance = sensorTag.Instance.ToUpper();
                            string _namespace = sensorTag.Namespace.ToUpper();

                            var organization =  _context.Organizations
                                .Where(o => o.GUID.ToUpper() == _namespace.ToUpper())
                                .FirstOrDefault();

                            int tagID = -1;

                            int.TryParse(_instance.Substring(4, 4), out tagID);

                            TagLink tagLink = _context.TagLinks
                                .Where(x => x.TagID == tagID && x.OrganizationID == organization.OrganizationID)
                                .FirstOrDefault();

                            tagLink.LastSeenDate = DateTime.Now;

                            _context.TagLinks.Update(tagLink);
                            _context.SaveChanges();


                            Tag tag = _context.Tags
                                   .Where(x => x.TagInstance == _instance && x.TagNamespace == _namespace)
                                   .FirstOrDefault();

                            if (tag != null)
                            {
                                if (sensorTag.EventDate > tag.LastSeenDate)
                                {
                                    tag.LastSeenDate = sensorTag.EventDate > tag.LastSeenDate ? sensorTag.EventDate : tag.LastSeenDate;
                                    tag.TagMAC = sensorTag.Mac;
                                    tag.TagInstance = sensorTag.Instance.Length > 1 ? sensorTag.Instance.ToUpper() : tag.TagInstance;
                                    tag.TagNamespace = sensorTag.Namespace.Length > 1 ? sensorTag.Namespace.ToUpper() : tag.TagNamespace;
                                    tag.TagUrl = sensorTag.Url ?? tag.TagUrl;
                                }

                                tag.LastSeenDate = DateTime.UtcNow;

                                _context.Tags.Update(tag);

                                TagDetail info = new TagDetail
                                {
                                    OrganizationScannerID = organization.OrganizationID,
                                    TagID = tag.TagID,
                                    RSSI = sensorTag.Rssi,
                                    RecordDate = sensorTag.EventDate
                                };

                                _context.TagDetails.Add(info);
                                _context.SaveChanges();
                            }
                            else
                            {
                                if (sensorTag.Instance.Length < 1)
                                    continue;

                                string _type = sensorTag.Instance.Substring(0, 4);
                                tag = new Tag
                                {
                                    TagMAC = sensorTag.Mac,
                                    TagNamespace = sensorTag.Namespace.ToUpper(),
                                    TagInstance = sensorTag.Instance.ToUpper(),
                                    TagUrl = sensorTag.Url,
                                    TagTypeID = _type.Equals("1000") ? 1 : _type.Equals("2000") ? 2 : 3,
                                    CreateDate = DateTime.UtcNow,
                                    LastSeenDate = DateTime.UtcNow,
                                    IsActive = true
                                };

                                _context.Tags.Add(tag);
                                _context.SaveChanges();

                                TagDetail info = new TagDetail
                                {
                                    OrganizationScannerID = organization.OrganizationID,
                                    TagID = tag.TagID,
                                    RSSI = sensorTag.Rssi,
                                    RecordDate = sensorTag.EventDate
                                };

                                _context.TagDetails.Add(info);
                                _context.SaveChanges();
                            }
                        }
                    }
                    else
                    {
                        var organizationScanner = _context.OrganizationScanners.Where(x => x.GUID == scannerGUID.ToUpper()).FirstOrDefault();

                        if (organizationScanner != null && organizationScanner.OrganizationScannerID > 0)
                        {
                            organizationScanner.LastComunicationDate = DateTime.UtcNow;
                            _context.Update(organizationScanner);
                            _context.SaveChanges();

                            foreach (SensorTag st in datalist)
                            {

                                string _instance = st.Instance.ToUpper();
                                string _namespace = st.Namespace.ToUpper();

                                int tagID = -1;

                                int.TryParse(_instance.Substring(4, 4), out tagID);

                                TagLink tagLink = _context.TagLinks
                                    .Where(x => x.TagID == tagID && x.OrganizationID == organizationScanner.OrganizationID)
                                    .FirstOrDefault();

                                tagLink.LastSeenDate = DateTime.Now;

                                _context.TagLinks.Update(tagLink);
                                _context.SaveChanges();

                                Tag tag = _context.Tags
                                    .Where(x => x.TagInstance == _instance && x.TagNamespace == _namespace)
                                    .FirstOrDefault();

                                if (tag != null)
                                {
                                    if (st.EventDate > tag.LastSeenDate)
                                    {
                                        tag.LastSeenDate = st.EventDate > tag.LastSeenDate ? st.EventDate : tag.LastSeenDate;
                                        tag.TagMAC = st.Mac;
                                        tag.TagInstance = st.Instance.Length > 1 ? st.Instance.ToUpper() : tag.TagInstance;
                                        tag.TagNamespace = st.Namespace.Length > 1 ? st.Namespace.ToUpper() : tag.TagNamespace;
                                        tag.TagUrl = st.Url.Length > 1 ? st.Url : tag.TagUrl;
                                    }

                                    tag.LastSeenDate = DateTime.UtcNow;

                                    _context.Tags.Update(tag);

                                    TagDetail info = new TagDetail
                                    {
                                        OrganizationScannerID = organizationScanner.OrganizationScannerID,
                                        TagID = tag.TagID,
                                        RSSI = st.Rssi,
                                        RecordDate = st.EventDate
                                    };

                                    _context.TagDetails.Add(info);
                                    _context.SaveChanges();
                                }
                                else
                                {
                                    if (st.Instance.Length < 1)
                                        continue;

                                    string _type = st.Instance.Substring(0, 4);
                                    tag = new Tag
                                    {
                                        TagMAC = st.Mac,
                                        TagNamespace = st.Namespace.ToUpper(),
                                        TagInstance = st.Instance.ToUpper(),
                                        TagUrl = st.Url,
                                        TagTypeID = _type.Equals("1000") ? 1 : _type.Equals("2000") ? 2 : 3,
                                        CreateDate = DateTime.UtcNow,
                                        LastSeenDate = DateTime.UtcNow,
                                        IsActive = true
                                    };

                                    _context.Tags.Add(tag);
                                    _context.SaveChanges();

                                    TagDetail info = new TagDetail
                                    {
                                        OrganizationScannerID = organizationScanner.OrganizationScannerID,
                                        TagID = tag.TagID,
                                        RSSI = st.Rssi,
                                        RecordDate = st.EventDate
                                    };

                                    _context.TagDetails.Add(info);
                                    _context.SaveChanges();
                                }
                            }
                        }
                    }

                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                }
            }
        }

        public List<RouteAndroid> GetRouteAndroidData(DateTime date)
        {
            List<RouteAndroid> result = new List<RouteAndroid>();

            var organizations = _context.Organizations.ToList();

            List<TagLink> tagLinks = _context.TagLinks
             .Where(tl => tl.TagLinkTypeID == (int)TagLinkTypes.Bus || tl.TagLinkTypeID == (int)TagLinkTypes.Trolley)
             .ToList();

            foreach(var organization in organizations) 
            { 

                var dailySchedule = _context.DailySchedules
                    .Where(ds => ds.OrganizationID == organization.OrganizationID)
                    .OrderByDescending(x => x.UpdateDate)
                    .FirstOrDefault();

                if (dailySchedule == null)
                    continue;

                List<TransportTrip> transportTrips = _context.TransportTrips
                    .Where(tt => tt.DailyTransportTripID == dailySchedule.DailyTransportTripID)
                    .ToList();

                foreach (var transportTrip in transportTrips)
                {
                    if (transportTrip.EndDate < date)
                        continue;

                    RouteAndroid routeAndroid = new RouteAndroid
                    {
                        OrganizationId = dailySchedule.OrganizationID,
                        Route = transportTrip.RouteName != null ? transportTrip.RouteName : "",
                        TransportId = transportTrip.ClientSystemID.ToString(),
                        TagNo = String.Empty,
                        StartDate = transportTrip.StartDate.ToString("yyyyMMdd HH:mm:ss"),
                        EndDate = transportTrip.EndDate.ToString("yyyyMMdd HH:mm:ss"),
                        RouteId = transportTrip.DirectionID != null ? transportTrip.DirectionID : "",
                        RouteName = transportTrip.DirectionName != null ? transportTrip.DirectionName : "",
                        ContentLithuania = transportTrip.ContentLithuania,
                        ShortContentLithuania = transportTrip.ShortContentLithuania,
                        ContentEnglish = transportTrip.ContentEnglish,
                        ShortContentEnglish = transportTrip.ShortContentEnglish,
                        PlayList = transportTrip.PlayList,
                        ShortPlayList = transportTrip.ShortPlayList
                    };

                    int _TransportId;
                    Int32.TryParse(transportTrip.ClientSystemID, out _TransportId);

                    if (dailySchedule.OrganizationID == 1)
                    {
                        routeAndroid.TagNo = _TransportId.ToString();
                        result.Add(routeAndroid);
                    }
                    else
                    {
                        TagLink tagLink = tagLinks.Where(x => x.ClientSystemID.Equals(_TransportId.ToString())
                                && x.TagLinkTypeID == 1
                                && x.OrganizationID == dailySchedule.OrganizationID).FirstOrDefault();

                        if (tagLink != null)
                            routeAndroid.TagNo = tagLink.TagID.ToString();

                        result.Add(routeAndroid);
                    }
                }
            }
            return result;
        }

        public List<StationAndroid> GetStationAndroidList(DateTime dateTime)
        {
            List<StationAndroid> result = new List<StationAndroid>();

            List<TagLink> tagLinks = _context.TagLinks
                .Where(tl => tl.TagLinkTypeID == (int)TagLinkTypes.Station)
                .ToList();

            List<Station> stations =
                _context.Stations.ToList();

            foreach (var station in stations)
            {
                StationAndroid stationAndroid = new StationAndroid()
                {
                    StationId = station.ClientSystemID,
                    OrganizationId = station.OrganizationID,
                    StationName = station.StationName,
                    Latitude = station.Latitude.ToString(),
                    Longitude = station.Longitude.ToString(),
                    PlayList = station.PlayList,
                    ContentEnglish = station.ContentEnglish,
                    ContentLithuania = station.ContentLithunian,
                    Direction = station.StationDirection
                };

                if (station.ClientSystemID != null)
                {
                    TagLink tagLink = tagLinks.Where(x => x.ClientSystemID == station.ClientSystemID
                        && x.TagLinkTypeID == 3
                        && x.OrganizationID == station.OrganizationID).FirstOrDefault();

                    if (tagLink != null)
                        stationAndroid.TagNo = tagLink.TagID.ToString();
                }
                result.Add(stationAndroid);
            }
            return result;
        }
    }
}
