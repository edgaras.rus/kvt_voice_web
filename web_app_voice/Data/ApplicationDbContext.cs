﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using web_app_voice.Models;
using web_app_voice.Models.Audio;
using web_app_voice.Models.Organizations;
using web_app_voice.Models.System;
using web_app_voice.Models.Tags;
using web_app_voice.Models.Transports;

namespace web_app_voice.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var config = new ConfigurationBuilder()
                    .AddEnvironmentVariables()
                    .Build();

                optionsBuilder.UseNpgsql("Host=personalizavimas.kvt.lt;Port=5444;Database=voice;Username=postgres;Password=d3v3l0p3r1!");
            }

            base.OnConfiguring(optionsBuilder);
        }
        */

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("Host=personalizavimas.kvt.lt;Port=5444;Database=voice;Username=postgres;Password=d3v3l0p3r1!");

    

        public DbSet<User> Users { get; set; }

        public DbSet<AudioType> AudioTypes { get; set; }

        public DbSet<Audio> Audios { get; set; }


        public DbSet<Organization> Organizations { get; set; }

        public DbSet<OrganizationScanner> OrganizationScanners { get; set; }


        public DbSet<TagType> TagTypes { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<TagDetail> TagDetails { get; set; }

        public DbSet<TagLinkType> TagLinkTypes { get; set; }

        public DbSet<TagLink> TagLinks { get; set; }


        public DbSet<Station> Stations { get; set; }

        public DbSet<DailySchedule> DailySchedules { get; set; }

        public DbSet<TransportTrip> TransportTrips { get; set; }


        public DbSet<RepetitiveTask> RepetitiveTasks { get; set; }

        public DbSet<RepetitiveTaskType> RepetitiveTaskTypes { get; set; }
    }
}
