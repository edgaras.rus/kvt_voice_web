﻿$.ajaxSetup({
    type: 'POST',
    cache: false,
    async: true,
    error: function (x) {
        if (x.statusText == 'abort') {
            return;
        } else if (x.status == 401) {
            swal({
                text: x.responseText,
                icon: "error",
                buttons: {
                    cancel: 'Close'
                }
            }).then(() => {
                document.location.href = "/";
            });
        } else {
            swal({
                text: x.responseText,
                icon: "error",
                buttons: {
                    cancel: 'Close'
                }
            });
        }
    },
});
/* jquery functions */
(
    function ($) {
        //show loader in input
        $.fn.inputLoader = function (action) {
            if (action == 'show') {
                $(this).addClass('app-input-loader');
            } else {
                $(this).removeClass('app-input-loader');
            }
        };

        //alert message from validationResult class
        $.alertValidationError = function (validationResult) {
            var errorMessages = "";

            try {
                var seperator = "";

                validationResult.errors.forEach(function (entry) {
                    errorMessages += seperator;
                    errorMessages += entry.errorMessage;
                    seperator = "\r\n";
                });
            } catch (e) {
                errorMessages = validationResult;
            }

            swal({
                text: errorMessages,
                buttons: {
                    cancel: 'OK'
                }
            });
        }

        $.fn.alertValidationError = function (validationResult) {
            var errorMessages = "";

            try {
                var seperator = "";

                validationResult.errors.forEach(function (entry) {
                    errorMessages += seperator;
                    errorMessages += entry.errorMessage;
                    seperator = "\r\n";
                });
            } catch (e) {
                errorMessages = validationResult;
            }

            swal({
                text: errorMessages,
                buttons: {
                    cancel: 'OK'
                }
            }).then(() => {
                this.focus();
            });
        }

        // format error from validationResult for edit form
        $.formatErrorResultForEdit = function (responseText) {
            try {
                var result = null;

                if (typeof responseText == 'object') {
                    result = responseText;
                } else {
                    result = JSON.parse(responseText);
                }

                var errMsg = "";
                var seperator = "";

                result.errors.forEach(function (entry) {
                    errMsg += seperator;
                    errMsg += entry.errorMessage;

                    if (entry.columnName) {
                        $('#' + entry.columnName).addClass('input-validation-error');
                    }

                    seperator = "<br />";
                });

                return [!result.hasError, errMsg, null];
            } catch (e) {
                return [false, responseText, null];
            }
        };


        //add/remove input error style
        $.fn.setInputError = function () {
            this.addClass('input-validation-error');
        }

        $.fn.removeInputError = function () {
            this.removeClass('input-validation-error');
        }

        // Sucentruojame redagavimo forma
        $.fn.centerModal = function () {
            this.css("position", "absolute");

            doCenterModal(this);

            var $meOnResize = this;

            $(window).resize(function () {
                doCenterModal($meOnResize);
            });
        };

        function doCenterModal($control) {
            $control.css("left", ($(window).width() - $control.outerWidth()) / 2 + $(window).scrollLeft() + "px");

            var windowH = $(window).height();
            var popupH = $control.outerHeight();
            var hDiff = windowH - popupH;

            if (windowH < popupH) {
                $control.css('top', '2px');
            } else {
                hDiff = hDiff > 280 ? 280 : hDiff;
                $control.css('top', '' + (hDiff / 2 + $(window).scrollTop()) + 'px');
            }
        };

        // Kazkoks spinerio palengvinimas 
        $.fn.skSpinner = function (aciotn) {

            if (aciotn == 'show') {
                this.parent().addClass('sk-loading');
            } else {
                this.parent().removeClass('sk-loading');
            }

        }

        // kilinam ajax call'a
        $.abortAjax = function (xhr) {
            if (xhr && xhr.readystate != 4) {
                xhr.abort();
            }
        }
    }
)(jQuery);