﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.AudiosDto
{
    public class AudioDto
    {
        public int AudioID { get; set; }

        public int OrganizationID { get; set; }

        public string OrganizationName { get; set; }

        public int AudioTypeID { get; set; }

        public string AudioTypeName { get; set; }

        public string SystemName { get; set; }

        public string SpeakOutLTU { get; set; }

        public string SpeakOutEN { get; set; }

        public bool Active { get; set; }

        public byte [] Bytes { get; set; }

        public string AudioBase64 { get; set; }

        public DateTime UpdateDate { get; set; }
    }
}
