﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.System
{

    [Table("RepetitiveTaskTypes", Schema = "systems")]

    public class RepetitiveTaskType
    {
        [Key]

        public int RepetitiveTaskTypeID { get; set; }

        public string Name { get; set; }
    }
}
