﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Tags
{

    [Table("TagTypes", Schema = "tags")]
    public class TagType
    {
        [Key]
        public int TagTypeID { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }
    }
}
