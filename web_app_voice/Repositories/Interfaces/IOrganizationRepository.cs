﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Models.Organizations;

namespace web_app_voice.Repositories.Interfaces
{
    public interface IOrganizationRepository
    {
        List<Organization> LoadOrganizations();

        Organization LoadOrganization(int organizationID);

        List<SelectListItem> GetOrganizationsForDropDown(int? organizationID);
    }
}
