﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto
{
    public class DashBoardDto
    {
        public string OrganizationName { get; set; }

        public string OrganizationLogo { get; set; }

        public DateTime LastTransportRoutesUpdateTime { get; set; }

        public DateTime LastStationsUpdateList { get; set; }

        public List<DashBoardScannerDto> OrganizationScaners { get; set; }


        public DashBoardDto()
        {
            LastTransportRoutesUpdateTime = DateTime.MinValue;
            LastStationsUpdateList = DateTime.MinValue;

            this.OrganizationLogo = string.Empty;
            this.OrganizationScaners = new List<DashBoardScannerDto>();
        }


    }
}
