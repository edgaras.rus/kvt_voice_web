﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace web_app_voice.Migrations
{
    public partial class ElapsedMillisecondsToRepetitiveTasks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ElapsedMilliseconds",
                schema: "systems",
                table: "RepetitiveTasks",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ElapsedMilliseconds",
                schema: "systems",
                table: "RepetitiveTasks");
        }
    }
}
