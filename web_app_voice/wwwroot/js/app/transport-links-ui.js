﻿"use strict";

var tagLinksValues = function (options) {

    var vars = {
        table: {},
        linkTypeID: 0,
        lastSeenTypeID: 0,
    }

    var root = this;

    this.construct = function (options) {
        $.extend(vars, options);

        initDataTable();

        var placeholderElement = $('#modal-placeholder')


        $("#newLink").on('click', function () {
            $.ajax({
                url: vars.createEditModalUrl,
                type: "POST",
                dataType: "text",
                data: {
                  
                },

                error: function (response) {
                    if (!response.Success) {
                        swal("Oops...!", "Something went wrong!", "error");
                    }
                },

                success: function (response) {
                    placeholderElement.html(response);
                    placeholderElement.find('.modal').modal('show');
                }
            });
        });

        $('#dataTable tbody').on('click', '.dataEdit', function () {
            var data = vars.table.row($(this).parents('tr')).data();

            $.ajax({
                url: vars.createEditModalUrl,
                type: "POST",
                dataType: "text",
                data: {
                    linkID: data.tagLinkID,
                },

                error: function (response) {
                    if (!response.Success) {
                        swal("Oops...!", "Something went wrong!", "error");
                    }
                },

                success: function (response) {
                    placeholderElement.html(response);
                    placeholderElement.find('.modal').modal('show');
                }
            });
        });

        $('#dataTable tbody').on('click', '.dataInfo', function () {
            var data = vars.table.row($(this).parents('tr')).data();

            $.ajax({
                url: vars.detailsModalUrl,
                type: "POST",
                dataType: "text",
                data: {
                    tagLinkID: data.tagLinkID,
                },

                error: function (response) {
                    if (!response.Success) {
                        swal("Oops...!", "Something went wrong!", "error");
                    }
                },

                success: function (response) {
                    placeholderElement.html(response);
                    placeholderElement.find('.modal').modal('show');
                }
            });

        });

        $('#dataTable tbody').on('click', '.dataRemove', function () {
            var data = vars.table.row($(this).parents('tr')).data();

            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this tag link.. Remember your actions always have inpact on pasangers !",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {

                $.ajax({
                    url: vars.removeLink,
                    type: "POST",
                    dataType: "text",
                    data: {
                        dataID: data.tagLinkID,
                    },
                    error: function (response) {
                        if (!response.Success) {
                            swal("Oops...!", "Something went wrong!", "error");
                        }
                    },

                    success: function (response) {
                        if (response.hasError) {
                            var _error = "";

                            $.each(data.errors, function (index, value) {
                                if (value.columnName) {
                                    _error = value.errorMessage
                                } else {
                                    _error = value.errorMessage
                                }
                            });
                            swal("Oops...!", _error, "error");
                        } else {
                            vars.table.ajax.reload();
                            swal("Deleted!", "Your tag link has been deleted.", "success");
                        }
                    }
                });
            });
        });

        placeholderElement.on('click', '[data-save="modal"]', function (event) {
            event.preventDefault();

            var form = $(this).parents('.modal').find('form');
            var actionUrl = form.attr('action');
            var dataToSend = form.serialize();

            $.post(actionUrl, dataToSend).done(function (data) {
                if (data.hasError) {
                    $.each(data.errors, function (index, value) {
                        if (value.columnName) {
                            $('#' + value.columnName).addClass('input-validation-error');
                            $('#' + value.columnName + 'Error').html(value.errorMessage);
                            $('#' + value.columnName + 'Error').show();
                        } else {
                            $('#modalError').html(value.errorMessage);
                            $('#modalError').show();
                        }
                    });
                } else {
                    placeholderElement.find('.modal').modal('hide');
                    vars.table.ajax.reload();
                }
            });
        });
    }

    function reloadTable() {
        vars.table.ajax.reload();
    }

    function initDataTable() {
        vars.table = $("#dataTable").DataTable({

            dom: '<"html5buttons"B>lTfgitp',
            // Design Assets
            stateSave: true,
            autoWidth: true,
            // ServerSide Setups
            processing: true,
            serverSide: true,
            // Paging Setups
            paging: true,
            // Searching Setups
            searching: { regex: true },
            // Ajax


            ajax: {
                url: vars.getTableDataUrl,
                type: "POST",
                dataType: "json",
                data: function (d) {
                    var object = {
                        dtParameters: d,
                        organizationID: vars.organizationID,
                        lastSeenTypeID: vars.lastSeenTypeID,
                    }
                    return object;
                }
            },
            buttons: [
                {
                    extend: 'collection',
                    text: 'Last Seen Type',
                    buttons: [
                        { text: 'ALL', action: function () { vars.lastSeenTypeID = 0; reloadTable() } },
                        { text: 'Active', action: function () { vars.lastSeenTypeID = 1; reloadTable() } },
                        { text: 'Inactive', action: function () { vars.lastSeenTypeID = 2; reloadTable() } },
                        { text: 'Long inactive', action: function () { vars.lastSeenTypeID = 3; reloadTable() } },

                    ],
                    fade: true
                },

                { extend: 'copy' },
                { extend: 'csv' },
                { extend: 'excel', title: 'ExampleFile' },
                { extend: 'pdf', title: 'ExampleFile' },

                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ],

            columns: [
                { "width": "16%", data: "tagID", "name": "Tag ID" },
                { "width": "16%", data: "tagLinkTypeName", "name": "Tag type" },
                { "width": "16%", data: "clientSystemID", "name": "Client sys. ID" },
                { "width": "16%", data: "organizationName", "name": "Organization" },
                { "width": "16%", data: "lastSeenDate", "name": "Last seen date" },
                { "width": "16%", data: "lastPlanedDate", "name": "Last planed date" },
                {
                    data: "tagLinkID", width: "4%", "render": function (data, type, full, meta) {
                        return '<div class="btn-group">'
                            + '<button data-toggle="ajax-modal" data-target="#info-row" class="btn btn-info dataInfo" style="margin-right:5px"><i class="fa fa-info"></i></button>'
                            + '<button data-toggle="ajax-modal" data-target="#edit-row" class="btn btn-warning dataEdit" style="margin-right:5px"><i class="fa fa-edit"></i></button>'
                            + '<button class="btn btn-danger dataRemove"><i class="fa fa-remove"></i></button>'
                            + '</div>';
                    }


                    //fa-edit

                    //fa-info
                        //< button class= "btn btn-outline btn-success  dim" type="button" > <i class="fa fa-upload"></i></button>
                }
            ],

            columnDefs: [
                { targets: [0, 1, 2, 3, 4, 5], orderable: true },
                { targets: [6], orderable: false },

                { targets: "no-sort", orderable: false },
                { targets: "no-search", searchable: false },
                {
                    targets: "trim",
                    render: function (data, type, full, meta) {
                        if (type === "display") {
                            data = strtrunc(data, 10);
                        }
                        return data;
                    }
                }
            ]
        });
    }

    this.construct(options);
}