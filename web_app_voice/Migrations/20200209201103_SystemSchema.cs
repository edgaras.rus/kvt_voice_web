﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace web_app_voice.Migrations
{
    public partial class SystemSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "systems");

            migrationBuilder.AddColumn<string>(
                name: "DirectionName",
                schema: "transports",
                table: "TransportTrips",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "RepetitiveTasks",
                schema: "systems",
                columns: table => new
                {
                    RepetitiveTaskID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RepetitiveTaskTypeID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Frequency = table.Column<int>(nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    LastExecuteTime = table.Column<DateTime>(nullable: true),
                    NexExecuteTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepetitiveTasks", x => x.RepetitiveTaskID);
                });

            migrationBuilder.CreateTable(
                name: "RepetitiveTaskTypes",
                schema: "systems",
                columns: table => new
                {
                    RepetitiveTaskTypeID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepetitiveTaskTypes", x => x.RepetitiveTaskTypeID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RepetitiveTasks",
                schema: "systems");

            migrationBuilder.DropTable(
                name: "RepetitiveTaskTypes",
                schema: "systems");

            migrationBuilder.DropColumn(
                name: "DirectionName",
                schema: "transports",
                table: "TransportTrips");
        }
    }
}
