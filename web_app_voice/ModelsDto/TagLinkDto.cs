﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto
{
    public class TagLinkDto
    {
        public int TagLinkID { get; set; }

        public int OrganizationID { get; set; }

        public string OrganizationName { get; set; }

        public int TagLinkTypeID { get; set; }

        public string TagLinkTypeName { get; set; }

        public int TagID { get; set; }

        public string ClientSystemID { get; set; }

        public string ClientSystemName { get; set; }
        
        public DateTime? LastSeenDateTime { get; set; }

        public DateTime? LastPlanedDateTime { get; set; }

        public string LastSeenDate { get; set; }

        public string LastPlanedDate { get; set; }

        public TagLinkDto()
        {
           
            this.LastSeenDateTime = null;
            this.LastPlanedDateTime = null;

            this.LastSeenDate = "No Data!";
            this.LastPlanedDate = "No Data!";
            this.ClientSystemName = "No Data!";
        }
    }
}
