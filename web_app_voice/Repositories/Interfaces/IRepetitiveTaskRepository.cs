﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Models.System;

namespace web_app_voice.Repositories.Interfaces
{
    public interface IRepetitiveTaskRepository
    {
        List<RepetitiveTask> LoadRepetitiveTasks(bool inactive = false);

        void UpdateRepetitiveTask(RepetitiveTask repetitiveTask);
    }
}
