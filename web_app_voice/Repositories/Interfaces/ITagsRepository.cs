﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Models.Tags;
using web_app_voice.ModelsDto.AndroidDto;
using web_app_voice.ModelsDto.SensorDto;
using web_app_voice.ModelsDto.TagsDto;

namespace web_app_voice.Repositories.Interfaces
{
    public interface ITagsRepository
    {
        void SaveSensorData(List<SensorTag> datalist, string scannerGUID = null);

        List<RouteAndroid> GetRouteAndroidData(DateTime date);

        List<StationAndroid> GetStationAndroidList(DateTime dateTime);

        List<TagDto> LoadTagListDto(TagsFilterDto filter, int? organizationID);

        TagDetailDto LoadTagDetailDto(int tagID, DateTime start, DateTime end);

        List<Tag> GetTags();
    }
}
