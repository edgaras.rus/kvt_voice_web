﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using web_app_voice.Extensions;
using web_app_voice.Infrastructure.AppValidations;
using web_app_voice.Models;
using web_app_voice.ModelsDto;
using web_app_voice.Repositories.Interfaces;
using static web_app_voice.ModelsDto.DataTables.DatatableModels;

namespace web_app_voice.Controllers
{
    [Authorize]
    public class TransportLinksController : Controller
    {
        private readonly ILinksRepository _linksRepository;
        private readonly IOrganizationRepository _organizationRepository;
        private readonly UserManager<User> _userManager;


        public TransportLinksController(ILinksRepository linksRepository,
            IOrganizationRepository organizationRepository, UserManager<User> userManager)
        {
            this._linksRepository = linksRepository;
            this._organizationRepository = organizationRepository;
            this._userManager = userManager;
        }

        public IActionResult Index()
        {
            ViewData["OrganizationID"] = GetUser().OrganizationID;
            return View();
        }

        [HttpPost]

        public IActionResult TransportLinkDetails(int tagLinkID)
        {
            try
            {
                TransportLinkDetailsDto transportLinkDetailsDto = _linksRepository.GetTransportLinkDetailsDto(tagLinkID);

                return PartialView("~/Views/TransportLinks/Modals/_TransportLinkDetailsPartial.cshtml", transportLinkDetailsDto);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        [HttpPost]
        public IActionResult CreateEditModal(int? linkID)
        {
            try
            {
                TagLinkCreateEditDto tagLinkCreateEditDto = new TagLinkCreateEditDto();
                if (linkID.HasValue)
                {
                    tagLinkCreateEditDto = _linksRepository.LoadTagLinkCreateEditDto(linkID.Value);
                }
                tagLinkCreateEditDto.Organizations = _organizationRepository.GetOrganizationsForDropDown(GetUser().OrganizationID);
                tagLinkCreateEditDto.TagLinkTypes = new List<SelectListItem>() {
                    new SelectListItem
                    {
                        Text ="Bus",
                        Value = "1"
                    },
                    new SelectListItem
                    {
                        Text ="Trolley",
                        Value = "1"
                    },


                };

                return PartialView("~/Views/Links/Modals/_CreateEditLinkPartial.cshtml", tagLinkCreateEditDto);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public IActionResult LoadDataTable(DTParameters dtParameters, int? organizationID = null, int? lastSeenTypeID = null)
        {
            var searchBy = dtParameters.Search?.Value;

            var orderCriteria = string.Empty;
            var orderAscendingDirection = true;

            if (dtParameters.Order != null)
            {
                orderCriteria = dtParameters.Columns[dtParameters.Order[0].Column].Data;
                orderAscendingDirection = dtParameters.Order[0].Dir.ToString().ToLower() == "asc";
            }
            else
            {
                orderCriteria = "TagID";
                orderAscendingDirection = true;
            }

            var result = _linksRepository.LoadTagLinksByType(organizationID, 1);

            var totalResultsCount = result.Count();

            if (!string.IsNullOrEmpty(searchBy))
            {
                result = result.Where(r => r.ClientSystemID != null
                    && r.ClientSystemID.ToUpper().Contains(searchBy.ToUpper())).ToList();
            }

            if (lastSeenTypeID.HasValue)
            {
                switch (lastSeenTypeID)
                {
                    case 0:
                        break;

                    case 1:
                        result = result.Where(x => x.LastSeenDateTime.HasValue
                            && x.LastSeenDateTime.Value > DateTime.Now.AddDays(-1)).ToList();
                        break;

                    case 2:
                        result = result.Where(x => x.LastSeenDateTime.HasValue
                            && x.LastSeenDateTime.Value < DateTime.Now.AddDays(-1)
                            && x.LastSeenDateTime.Value > DateTime.Now.AddDays(-5)).ToList();
                        break;

                    case 3:
                        result = result.Where(x => !x.LastSeenDateTime.HasValue
                            || (x.LastSeenDateTime.HasValue && x.LastSeenDateTime.Value < DateTime.Now.AddDays(-5))).ToList();
                        break;

                    default:
                        break;

                }
            }

            result = orderAscendingDirection ? result.AsQueryable().OrderByDynamic(orderCriteria, LinqExtensions.Order.Asc).ToList()
                : result.AsQueryable().OrderByDynamic(orderCriteria, LinqExtensions.Order.Desc).ToList();

            // now just get the count of items (without the skip and take) - eg how many could be returned with filtering
            var filteredResultsCount = result.Count();

            return Json(new
            {
                draw = dtParameters.Draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
                    .Skip(dtParameters.Start)
                    .Take(dtParameters.Length)
                    .ToList()
            });
        }

        [HttpPost]
        public IActionResult SaveModalData(TagLinkCreateEditDto tagLinkCreateEditDto)
        {
            var result = ModelState.GetErrors(true);

            if (result.HasError)
                return result.JsonResult();


            if (tagLinkCreateEditDto.TagLinkID > 0)
            {
                if (_linksRepository.IsLinkWithClientSystemIDExists(tagLinkCreateEditDto.ClientSystemID,
                    tagLinkCreateEditDto.TagLinkTypeID, tagLinkCreateEditDto.OrganizationID, tagLinkCreateEditDto.TagLinkID))
                {
                    return new ValidationResult(new PropertyError(string.Format("Tag link cannot be updated, becouse other exists with same Client System ID {0}",
                        tagLinkCreateEditDto.ClientSystemID))).JsonResult();
                }

                if (_linksRepository.IsLinkWithTagIDExists(tagLinkCreateEditDto.TagID.ToString(), tagLinkCreateEditDto.TagLinkTypeID,
                    tagLinkCreateEditDto.OrganizationID, tagLinkCreateEditDto.TagLinkID))
                {
                    return new ValidationResult(new PropertyError(string.Format("Tag link cannot be updated, becouse other exists with same Tag ID {0}",
                      tagLinkCreateEditDto.TagID.ToString()))).JsonResult();
                }
            }
            else
            {
                if (_linksRepository.IsLinkWithClientSystemIDExists(tagLinkCreateEditDto.ClientSystemID,
                 tagLinkCreateEditDto.TagLinkTypeID, tagLinkCreateEditDto.OrganizationID, null))
                {
                    return new ValidationResult(new PropertyError(string.Format("Tag link cannot be created, becouse other exists with same Client System ID {0}",
                        tagLinkCreateEditDto.ClientSystemID))).JsonResult();
                }

                if (_linksRepository.IsLinkWithTagIDExists(tagLinkCreateEditDto.TagID.ToString(), tagLinkCreateEditDto.TagLinkTypeID,
                    tagLinkCreateEditDto.OrganizationID, null))
                {
                    return new ValidationResult(new PropertyError(string.Format("Tag link cannot be created, becouse other exists with same Tag ID {0}",
                      tagLinkCreateEditDto.TagID.ToString()))).JsonResult();
                }
            }

            _linksRepository.SaveUpdate(tagLinkCreateEditDto);

            return new ValidationResult().JsonResult();
        }

        public IActionResult RemoveData(int dataID)
        {
            try
            {
                _linksRepository.Remove(dataID);
            }
            catch (Exception e)
            {
                return new ValidationResult(new PropertyError(e.Message)).JsonResult();
            }
            return new ValidationResult().JsonResult();
        }

        private User GetUser()
        {
            return _userManager.FindByIdAsync(User.FindFirstValue(ClaimTypes.NameIdentifier)).Result;
        }


    }
}