﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Enums
{
    public enum TagStateTypes
    {
        [Description("Gerai")]
        Okey = 10,

        [Description("Perspėjimas")]
        Warrning = 20,

        [Description("Negerai")]
        Error = 30,
    }
}
