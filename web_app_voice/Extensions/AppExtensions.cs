﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Extensions
{
    public static class AppExtensions
    {
        public static string ToSafeString(this object obj)
        {
            return (obj ?? string.Empty).ToString().Trim();
        }

        public static bool StringIsEmpty(this string str)
        {
            return str == null || str.Trim() == "";
        }

        public static string ToFullErrorText(this Exception ex)
        {
            string error = ex.Message;
            var innerException = ex.InnerException;

            while (innerException != null)
            {
                error += $"\r\nInnerError: " + innerException.Message;
                innerException = innerException.InnerException;
            }

            return error;
        }
        /*

        #region Translation
        //---------------------------------------------------------------------------------------------------------------
        private const string KEYWORD_PREFIX = "#";

        public static string GetRequestCulture(this HttpContext httpContext)
        {
            var features = httpContext.Features.Get<IRequestCultureFeature>();

            return features.RequestCulture.Culture.Name.ToLower();
        }

        public static string TranslateByCulture(this string keyword, string cultureName)
        {
            var translations = MultiLanguageService.Translations[cultureName];

            if (translations.ContainsKey(keyword))
                return translations[keyword];
            else
                return KEYWORD_PREFIX + keyword;
        }

        private static Dictionary<string, string> GetTranslations()
        {
            string currentCulture = CultureInfo.CurrentCulture.Name.ToLower();
            var translations = MultiLanguageService.Translations[currentCulture];

            return translations;
        }

        public static string Translate(this string keyword)
        {
            var translations = GetTranslations();

            if (translations.ContainsKey(keyword))
                return translations[keyword];
            else
                return KEYWORD_PREFIX + keyword;
        }

        public static string Translate(this string keyword, params object[] args)
        {
            var translations = GetTranslations();

            if (translations.ContainsKey(keyword))
                return string.Format(translations[keyword], args);
            else
                return KEYWORD_PREFIX + keyword;
        }

        public static List<T> Translate<T>(this List<T> data, string translateColumn) where T : ICloneable
        {
            if (data == null || data.Count == 0) { return data; }

            var translations = GetTranslations();

            var newData = data.Select(x => (T)x.Clone()).ToList();
            var propInfo = newData[0].GetType().GetProperty(translateColumn);

            foreach (var item in newData)
            {
                string keyword = propInfo.GetValue(item).ToSafeString();
                if (keyword.StringIsEmpty()) { continue; }

                if (translations.ContainsKey(keyword))
                    propInfo.SetValue(item, translations[keyword]);
                else
                    propInfo.SetValue(item, KEYWORD_PREFIX + keyword);
            }

            return newData;
        }

        public static Dictionary<string, string> Translate(this string[] keywords)
        {
            var translations = GetTranslations();

            return keywords
                   .ToDictionary(x => x, x => translations.ContainsKey(x) ? translations[x] : KEYWORD_PREFIX + x);
        }



        private static Dictionary<string, string> GetModelValidationTranslations()
        {
            string currentCulture = CultureInfo.CurrentCulture.Name.ToLower();
            var translations = MultiLanguageService.ModelValidationTranslations[currentCulture];

            return translations;
        }

        public static string ModelValidationTranslate(this string keyword)
        {
            var translations = GetModelValidationTranslations();

            if (translations.ContainsKey(keyword))
                return translations[keyword];
            else
                return KEYWORD_PREFIX + keyword;
        }

        public static string ModelValidationTranslate(this string keyword, params object[] args)
        {
            var translations = GetModelValidationTranslations();

            if (translations.ContainsKey(keyword))
                return string.Format(translations[keyword], args);
            else
                return KEYWORD_PREFIX + keyword;
        }
        //---------------------------------------------------------------------------------------------------------------
        #endregion

    */
        #region Convert datetypes
        //---------------------------------------------------------------------------------------------------------------
        public static int? ToInt32(this object obj)
        {
            try
            {
                string str = obj.ToSafeString();

                if (string.IsNullOrEmpty(str))
                    return null;
                else
                    return Convert.ToInt32(str);
            }
            catch
            {
                return null;
            }
        }
        //---------------------------------------------------------------------------------------------------------------
        #endregion

        /*
        #region For select2
        //---------------------------------------------------------------------------------------------------------------
        public static string ToDropDown(this List<Select2Option> source)
        {
            var items = new List<string> { ":" };
            items.AddRange(source.Select(x => $"{x.Value}:{x.Text}").ToList());

            return string.Join(";", items);
        }

        public static JsonResult ToJsonDropDown(this List<Select2Option> source)
        {
            return new JsonResult(source.ToDropDown());
        }
        //---------------------------------------------------------------------------------------------------------------
        #endregion
        */
        public static void SetTitle(this Controller controller, string titleKeyword)
        {
            //controller.ViewData["Title"] = titleKeyword.Translate();
            controller.ViewData["Title"] = titleKeyword;
        }

        public static T JsonToObject<T>(this string param)
        {
            var settings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };

            return JsonConvert.DeserializeObject<T>(param, settings);
        }

        /*
        public static void ExportToExcel<T>(this List<T> data, JqGridMainParams gridParams, string fileName, List<string> excludeColumns = null) where T : class
        {
            byte[] fileData = null;
            var dataType = typeof(T);
            int rowCount = data.Count;
            int colIndex = 1, rowIndex = 2;
            excludeColumns = excludeColumns ?? new List<string>();
            var translations = AppCore.Translate("valYes", "valNo");
            var colModel = gridParams.ColModels ?? new List<JqGridColumnModel>();

            var columns = dataType.GetProperties()
                          .Select(x => new
                          {
                              Caption = x.GetCustomAttributes(true)
                                        .Where(z => z != null && z.GetType() == typeof(AppDisplayAttribute))
                                        .Select(z => ((AppDisplayAttribute)z).DisplayName)
                                        .FirstOrDefault(),
                              x.Name,
                              PropertyType = x.MemberType.GetType(),
                              IsNullable = x.PropertyType.IsGenericType && x.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>),
                              Property = x
                          })
                          .Where(x => !excludeColumns.Contains(x.Name))
                          .Select(x => new
                          {
                              Caption = x.Caption ?? x.Name,
                              x.Name,
                              PropertyType = x.IsNullable ? Nullable.GetUnderlyingType(x.PropertyType) : x.PropertyType,
                              x.IsNullable,
                              x.Property
                          })
                          .ToList();

            using (ExcelPackage xls = new ExcelPackage())
            {
                using (var ws = xls.Workbook.Worksheets.Add("Sheet1"))
                {
                    foreach (var column in columns)
                    {
                        var myColModel = colModel.FirstOrDefault(x => x.ColumnName.ToUpper() == column.Name.ToUpper());
                        if (myColModel == null) { continue; }

                        ws.SetValue(1, colIndex, column.Caption);

                        foreach (var cell in data)
                        {
                            var value = column.Property.GetValue(cell, null);

                            if (column.PropertyType == typeof(bool))
                                value = (bool?)value == true ? translations["valYes"] : translations["valNo"];

                            ws.SetValue(rowIndex, colIndex, value);
                            rowIndex++;
                        }

                        // stulpelio plotis 
                        if (myColModel.ColumnWidth.HasValue)
                            ws.Column(colIndex).Width = (double)myColModel.ColumnWidth.Value / 7d + 3.5;
                        else
                            ws.Column(colIndex).Width = 10;

                        using (var columnRange = ws.Cells[2, colIndex, rowIndex - 1, colIndex])
                        {
                            var colStyle = columnRange.Style;

                            // Teksto lygiavimas
                            switch (myColModel.TextAligment)
                            {
                                case "center":
                                    colStyle.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    break;

                                case "right":
                                    colStyle.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    break;

                                default:
                                    colStyle.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    break;
                            }
                        }

                        colIndex++;
                        rowIndex = 2;
                    }

                    Color borderColor = Color.FromArgb(160, 160, 160);

                    // Headeris
                    using (var headerRange = ws.Cells[1, 1, 1, colIndex - 1])
                    {
                        var hStyle = headerRange.Style;
                        hStyle.Font.Bold = true;
                        hStyle.WrapText = true;
                        hStyle.VerticalAlignment = ExcelVerticalAlignment.Center;
                        hStyle.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hStyle.Fill.PatternType = ExcelFillStyle.Solid;
                        hStyle.Fill.BackgroundColor.SetColor(Color.FromArgb(190, 190, 190));
                        hStyle.Border.BorderAround(ExcelBorderStyle.Thin, borderColor);
                        hStyle.Border.Left.Style = ExcelBorderStyle.Thin;
                        hStyle.Border.Left.Color.SetColor(borderColor);
                    }

                    // Duomenys
                    using (var dataRange = ws.Cells[2, 1, rowCount + 1, colIndex - 1])
                    {
                        var style = dataRange.Style;
                        style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        style.WrapText = true;
                        style.Border.BorderAround(ExcelBorderStyle.Thin, borderColor);
                        style.Border.Left.Style = ExcelBorderStyle.Thin;
                        style.Border.Left.Color.SetColor(borderColor);
                        style.Border.Top.Style = ExcelBorderStyle.Thin;
                        style.Border.Top.Color.SetColor(borderColor);
                    }

                    // filtrai
                    ws.Cells[ws.Dimension.Address].AutoFilter = true;

                    fileData = xls.GetAsByteArray();
                }
            }

            // download
            WebContext.Current.Response.Headers.Clear();
            WebContext.Current.Response.Clear();
            WebContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            WebContext.Current.Response.Headers.Add("content-length", fileData.Length.ToString());
            WebContext.Current.Response.Headers.Add("content-disposition", "attachment;  filename=" + fileName + ".xlsx");
            WebContext.Current.Response.Body.WriteAsync(fileData, 0, fileData.Length);
        }
    */
    }
}

