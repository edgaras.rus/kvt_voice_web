﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Models.Organizations;

namespace web_app_voice.Models.Audio
{
    [Table("Audios", Schema = "audios")]
    public class Audio
    {
        [Key]
        public int AudioID { get; set; }

        [Required]
        public int OrganizationID { get; set; } 

        [Required]
        public string SystemName { get; set; }

        [Required]
        public int AudioTypeID { get; set; }

        public string SpeakOutLtu { get; set; }

        public string SpeakOutEn { get; set; }

        [DefaultValue(true)]
        public bool Active { get; set; }

        public byte[] Bytes { get; set; }

        public DateTime UpdateDate { get; set; }


        [ForeignKey("OrganizationID")]
        public virtual Organization Organization { get; set; }

        [ForeignKey("AudioTypeID")]
        public virtual AudioType AudioType { get; set; }


        public Audio()
        {
            UpdateDate = DateTime.Now;
        }

    }
}
