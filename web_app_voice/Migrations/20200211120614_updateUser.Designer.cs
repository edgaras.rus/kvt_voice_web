﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using web_app_voice.Data;

namespace web_app_voice.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20200211120614_updateUser")]
    partial class updateUser
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                .HasAnnotation("ProductVersion", "3.1.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("text");

                    b.Property<string>("Name")
                        .HasColumnType("character varying(256)")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasColumnType("character varying(256)")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("ClaimType")
                        .HasColumnType("text");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("text");

                    b.Property<string>("RoleId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUser", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("text");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("integer");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("text");

                    b.Property<string>("Discriminator")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("Email")
                        .HasColumnType("character varying(256)")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("boolean");

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("boolean");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("timestamp with time zone");

                    b.Property<string>("NormalizedEmail")
                        .HasColumnType("character varying(256)")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasColumnType("character varying(256)")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash")
                        .HasColumnType("text");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("text");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("boolean");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("text");

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("boolean");

                    b.Property<string>("UserName")
                        .HasColumnType("character varying(256)")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");

                    b.HasDiscriminator<string>("Discriminator").HasValue("IdentityUser");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("ClaimType")
                        .HasColumnType("text");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("text");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("text");

                    b.Property<string>("ProviderKey")
                        .HasColumnType("text");

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("text");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("text");

                    b.Property<string>("RoleId")
                        .HasColumnType("text");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("text");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("text");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.Property<string>("Value")
                        .HasColumnType("text");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("web_app_voice.Models.Audio.Audio", b =>
                {
                    b.Property<int>("AudioID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<bool>("Active")
                        .HasColumnType("boolean");

                    b.Property<int>("AudioTypeID")
                        .HasColumnType("integer");

                    b.Property<byte[]>("Bytes")
                        .HasColumnType("bytea");

                    b.Property<int>("OrganizationID")
                        .HasColumnType("integer");

                    b.Property<string>("SpeakOutEn")
                        .HasColumnType("text");

                    b.Property<string>("SpeakOutLtu")
                        .HasColumnType("text");

                    b.Property<string>("SystemName")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdateDate")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("AudioID");

                    b.HasIndex("AudioTypeID");

                    b.HasIndex("OrganizationID");

                    b.ToTable("Audios","audios");
                });

            modelBuilder.Entity("web_app_voice.Models.Audio.AudioType", b =>
                {
                    b.Property<int>("AudioTypeID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.HasKey("AudioTypeID");

                    b.ToTable("AudioTypes","audios");
                });

            modelBuilder.Entity("web_app_voice.Models.Organizations.Organization", b =>
                {
                    b.Property<int>("OrganizationID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Address")
                        .HasColumnType("text");

                    b.Property<bool>("Enable")
                        .HasColumnType("boolean");

                    b.Property<string>("GUID")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("PhoneNo")
                        .HasColumnType("text");

                    b.Property<string>("StationsDataUrl")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("TripsDataUrl")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("OrganizationID");

                    b.ToTable("Organizations","organizations");
                });

            modelBuilder.Entity("web_app_voice.Models.Organizations.OrganizationScanner", b =>
                {
                    b.Property<int>("OrganizationScannerID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("Description")
                        .HasColumnType("text");

                    b.Property<string>("GUID")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<DateTime>("LastComunicationDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("OrganizationID")
                        .HasColumnType("integer");

                    b.HasKey("OrganizationScannerID");

                    b.HasIndex("OrganizationID");

                    b.ToTable("OrganizationScanners","organizations");
                });

            modelBuilder.Entity("web_app_voice.Models.System.RepetitiveTask", b =>
                {
                    b.Property<int>("RepetitiveTaskID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<bool>("Enabled")
                        .HasColumnType("boolean");

                    b.Property<int>("Frequency")
                        .HasColumnType("integer");

                    b.Property<DateTime?>("LastExecuteTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.Property<DateTime?>("NexExecuteTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("RepetitiveTaskTypeID")
                        .HasColumnType("integer");

                    b.HasKey("RepetitiveTaskID");

                    b.ToTable("RepetitiveTasks","systems");
                });

            modelBuilder.Entity("web_app_voice.Models.System.RepetitiveTaskType", b =>
                {
                    b.Property<int>("RepetitiveTaskTypeID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.HasKey("RepetitiveTaskTypeID");

                    b.ToTable("RepetitiveTaskTypes","systems");
                });

            modelBuilder.Entity("web_app_voice.Models.Tags.Tag", b =>
                {
                    b.Property<int>("TagID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<bool>("IsActive")
                        .HasColumnType("boolean");

                    b.Property<DateTime>("LastSeenDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("TagInstance")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("TagMAC")
                        .HasColumnType("text");

                    b.Property<string>("TagNamespace")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("TagTypeID")
                        .HasColumnType("integer");

                    b.Property<string>("TagUrl")
                        .HasColumnType("text");

                    b.HasKey("TagID");

                    b.HasIndex("TagTypeID");

                    b.ToTable("Tags","tags");
                });

            modelBuilder.Entity("web_app_voice.Models.Tags.TagDetail", b =>
                {
                    b.Property<int>("TagDetailID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<int?>("OrganizationScannerID")
                        .HasColumnType("integer");

                    b.Property<int>("RSSI")
                        .HasColumnType("integer");

                    b.Property<DateTime>("RecordDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("TagID")
                        .HasColumnType("integer");

                    b.HasKey("TagDetailID");

                    b.HasIndex("TagID");

                    b.ToTable("TagDetails","tags");
                });

            modelBuilder.Entity("web_app_voice.Models.Tags.TagLink", b =>
                {
                    b.Property<int>("TagLinkID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("ClientSystemID")
                        .HasColumnType("text");

                    b.Property<int>("OrganizationID")
                        .HasColumnType("integer");

                    b.Property<int>("TagID")
                        .HasColumnType("integer");

                    b.Property<int>("TagLinkTypeID")
                        .HasColumnType("integer");

                    b.HasKey("TagLinkID");

                    b.HasIndex("OrganizationID");

                    b.HasIndex("TagLinkTypeID");

                    b.ToTable("TagLinks","tags");
                });

            modelBuilder.Entity("web_app_voice.Models.Tags.TagLinkType", b =>
                {
                    b.Property<int>("TagLinkTypeID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("TagLinkTypeID");

                    b.ToTable("TagLinkTypes","tags");
                });

            modelBuilder.Entity("web_app_voice.Models.Tags.TagType", b =>
                {
                    b.Property<int>("TagTypeID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Name")
                        .HasColumnType("character varying(255)")
                        .HasMaxLength(255);

                    b.HasKey("TagTypeID");

                    b.ToTable("TagTypes","tags");
                });

            modelBuilder.Entity("web_app_voice.Models.Transports.DailySchedule", b =>
                {
                    b.Property<int>("DailyTransportTripID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<int>("Added")
                        .HasColumnType("integer");

                    b.Property<DateTime>("DeclareDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("Eddited")
                        .HasColumnType("integer");

                    b.Property<int>("OrganizationID")
                        .HasColumnType("integer");

                    b.Property<int>("Removed")
                        .HasColumnType("integer");

                    b.Property<DateTime>("UpdateDate")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("DailyTransportTripID");

                    b.ToTable("DailySchedules","transports");
                });

            modelBuilder.Entity("web_app_voice.Models.Transports.Station", b =>
                {
                    b.Property<int>("StationID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("ClientSystemID")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("ContentEnglish")
                        .HasColumnType("text");

                    b.Property<string>("ContentLithunian")
                        .HasColumnType("text");

                    b.Property<double?>("Latitude")
                        .HasColumnType("double precision");

                    b.Property<double?>("Longitude")
                        .HasColumnType("double precision");

                    b.Property<int>("OrganizationID")
                        .HasColumnType("integer");

                    b.Property<string>("PlayList")
                        .HasColumnType("text");

                    b.Property<string>("ShortContentEnglish")
                        .HasColumnType("text");

                    b.Property<string>("ShortContentLithunian")
                        .HasColumnType("text");

                    b.Property<string>("ShortPlayList")
                        .HasColumnType("text");

                    b.Property<string>("StationDirection")
                        .HasColumnType("text");

                    b.Property<string>("StationName")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("StationID");

                    b.ToTable("Stations","transports");
                });

            modelBuilder.Entity("web_app_voice.Models.Transports.TransportTrip", b =>
                {
                    b.Property<int>("TransportTripID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("ClientSystemID")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("ContentEnglish")
                        .HasColumnType("text");

                    b.Property<string>("ContentLithuania")
                        .HasColumnType("text");

                    b.Property<int>("DailyTransportTripID")
                        .HasColumnType("integer");

                    b.Property<string>("DirectionID")
                        .HasColumnType("text");

                    b.Property<string>("DirectionName")
                        .HasColumnType("text");

                    b.Property<DateTime>("EndDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("PlayList")
                        .HasColumnType("text");

                    b.Property<string>("RouteName")
                        .HasColumnType("text");

                    b.Property<string>("ShortContentEnglish")
                        .HasColumnType("text");

                    b.Property<string>("ShortContentLithuania")
                        .HasColumnType("text");

                    b.Property<string>("ShortPlayList")
                        .HasColumnType("text");

                    b.Property<DateTime>("StartDate")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("TransportTripID");

                    b.HasIndex("DailyTransportTripID");

                    b.ToTable("TransportTrips","transports");
                });

            modelBuilder.Entity("web_app_voice.Models.User", b =>
                {
                    b.HasBaseType("Microsoft.AspNetCore.Identity.IdentityUser");

                    b.Property<bool>("IsActive")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsBlocked")
                        .HasColumnType("boolean");

                    b.Property<DateTime>("LastLoginTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int?>("OrganizationID")
                        .HasColumnType("integer");

                    b.HasDiscriminator().HasValue("User");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("web_app_voice.Models.Audio.Audio", b =>
                {
                    b.HasOne("web_app_voice.Models.Audio.AudioType", "AudioType")
                        .WithMany()
                        .HasForeignKey("AudioTypeID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("web_app_voice.Models.Organizations.Organization", "Organization")
                        .WithMany()
                        .HasForeignKey("OrganizationID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("web_app_voice.Models.Organizations.OrganizationScanner", b =>
                {
                    b.HasOne("web_app_voice.Models.Organizations.Organization", "Organization")
                        .WithMany("OrganizationScanners")
                        .HasForeignKey("OrganizationID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("web_app_voice.Models.Tags.Tag", b =>
                {
                    b.HasOne("web_app_voice.Models.Tags.TagType", "TagType")
                        .WithMany()
                        .HasForeignKey("TagTypeID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("web_app_voice.Models.Tags.TagDetail", b =>
                {
                    b.HasOne("web_app_voice.Models.Tags.Tag", "Tag")
                        .WithMany("TagDetails")
                        .HasForeignKey("TagID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("web_app_voice.Models.Tags.TagLink", b =>
                {
                    b.HasOne("web_app_voice.Models.Organizations.Organization", "Organization")
                        .WithMany()
                        .HasForeignKey("OrganizationID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("web_app_voice.Models.Tags.TagLinkType", "TagLinkType")
                        .WithMany()
                        .HasForeignKey("TagLinkTypeID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("web_app_voice.Models.Transports.TransportTrip", b =>
                {
                    b.HasOne("web_app_voice.Models.Transports.DailySchedule", "DailyTransportTrip")
                        .WithMany("TransportTrips")
                        .HasForeignKey("DailyTransportTripID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
