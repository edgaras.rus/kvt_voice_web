﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.AndroidDto
{
    public class StationBeaconsAndroid
    {
        public string Mac { get; set; }

        public string Instance { get; set; }

        public string NameSpace { get; set; }

        public string Rssi { get; set; }

        public string EventDate { get; set; }

        public string DeviceKey { get; set; }
    }
}
