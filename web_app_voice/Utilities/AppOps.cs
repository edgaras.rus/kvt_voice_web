﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Models.Enums;

namespace web_app_voice.Utilities
{
    public class AppOps
    {
        public static DateTime RouteTimeFromWebToDateTime(string source, DateTime TripDate)
        {
            double value;
            Double.TryParse(source, out value);

            if (value == 0)
                return TripDate;

            double hours = (value / 60);
            double minutes = (((value / 60) - hours) * 60);
            DateTime result = new DateTime(TripDate.Year, TripDate.Month, TripDate.Day, 0, 0, 0);
            result = result.AddHours(hours);
            result = result.AddMinutes(minutes);
            return result;
        }

        public static string GetRouteContentLithuanian(string RouteName, string DirectionName)
        {
            return String.Format("{0} {1}", RouteName, DirectionName);
        }

        public static string GetRouteContentLithuanianByType(string RouteName, string DirectionName, TransportTypes transportType)
        {
            return String.Format("{0} {1} {2}", RouteName, transportType == TransportTypes.Bus ? "Autobusas" : transportType == TransportTypes.Trolley ? "Troleibusas" : "", DirectionName);
        }

        public static string GetShortContentLithuania(string RouteName)
        {
            return String.Format("{0}", RouteName);
        }

        public static string GetShortContentLithuaniaByType(string RouteName, TransportTypes transportType)
        {
            return String.Format("{0} {1}", RouteName, transportType == TransportTypes.Bus ? "Autobusas" : transportType == TransportTypes.Trolley ? "Troleibusas" : "");
        }

        public static string GetConetntEnglish(string RouteName, string DirectionName)
        {
            return String.Format("{0} to {1}", RouteName, DirectionName);
        }

        public static string GetConetntEnglishByType(string RouteName, string DirectionName, TransportTypes transportType)
        {
            return String.Format("{0} {1} to {2}", RouteName, transportType == TransportTypes.Bus ? "Bus" : transportType == TransportTypes.Trolley ? "Trolley" : "", DirectionName);
        }

        public static string GetShortContentEnglish(string RouteName)
        {
            return String.Format("{0}", RouteName);
        }

        public static string GetShortContentEnglishByType(string RouteName, TransportTypes transportType)
        {
            return String.Format("{0} {1}", RouteName, transportType == TransportTypes.Bus ? "Bus" : transportType == TransportTypes.Trolley ? "Trolley" : "");
        }


        public static string GetTransportPlayListByType(string routeName, string directionName, TransportTypes transportType)
        {
            string typeFile = "";

            switch (transportType)
            {
                case TransportTypes.Bus:
                    typeFile = "sys_" + "bus";
                    break;

                case TransportTypes.Trolley:
                    typeFile = "sys_" + "trolley";
                    break;
                default:
                    break;
            }

            return String.Format("{0};{1};{2}", GetRouteNamePlayList(routeName), typeFile, "tr_" + CreateFileName(directionName));
        }

        public static string GetTransportShortPlayListByType(string routeName, TransportTypes transportType)
        {
            string typeFile = "";

            switch (transportType)
            {
                case TransportTypes.Bus:
                    typeFile = "sys_" + "bus";
                    break;

                case TransportTypes.Trolley:
                    typeFile = "sys_" + "trolley";
                    break;
                default:
                    break;
            }

            return String.Format("{0};{1}", GetRouteNamePlayList(routeName), typeFile);
        }


        public static string GetPlayList(string RouteName, string DirectionName)
        {
            return String.Format("{0};{1}", GetRouteNamePlayList(RouteName), "tr_" + CreateFileName(DirectionName));
        }


        public static string GetShortPlayList(string RouteName, string DirectionName)
        {
            return String.Format("{0}", GetRouteNamePlayList(RouteName));
        }

        private static string GetRouteNamePlayList(string RouteName)
        {
            char[] array = RouteName.ToCharArray();
            int routeNumber = 0;

            if (char.IsDigit(array[array.Length - 1]))
            {
                int.TryParse(RouteName, out routeNumber);
                if (routeNumber > 0)
                    return RouteNumbersPlayDic[routeNumber];
            }
            else
            {
                int.TryParse(RouteName.Substring(0, array.Length - 1), out routeNumber);
                if (routeNumber > 0)
                    return string.Format("{0};{1}", RouteNumbersPlayDic[routeNumber], GetLetterPlayList(array[array.Length - 1].ToString()));
            }

            return "";
        }

        private static string GetLetterPlayList(string Letter)
        {
            Letter = "sys_" + Letter.ToLower();
            return Letter;
        }

     

        private static Dictionary<int, string> RouteNumbersPlayDic = new Dictionary<int, string>()
        {
            { 1, "sys_1as"},
            { 2, "sys_2as"},
            { 3, "sys_3as"},
            { 4, "sys_4as"},
            { 5, "sys_5as"},
            { 6, "sys_6as"},
            { 7, "sys_7as"},
            { 8, "sys_8as"},
            { 9, "sys_9as"},
            { 10, "sys_10as"},

            { 11, "sys_11as"},
            { 12, "sys_12as"},
            { 13, "sys_13as"},
            { 14, "sys_14as"},
            { 15, "sys_15as"},
            { 16, "sys_16as"},
            { 17, "sys_17as"},
            { 18, "sys_18as"},
            { 19, "sys_19as"},

            { 20, "sys_20as"},
            { 21, "sys_20;sys_1as"},
            { 22, "sys_20;sys_2as"},
            { 23, "sys_20;sys_3as"},
            { 24, "sys_20;sys_4as"},
            { 25, "sys_20;sys_5as"},
            { 26, "sys_20;sys_6as"},
            { 27, "sys_20;sys_7as"},
            { 28, "sys_20;sys_8as"},
            { 29, "sys_20;sys_9as"},

            { 30, "sys_30as"},
            { 31, "sys_30;sys_1as"},
            { 32, "sys_30;sys_2as"},
            { 33, "sys_30;sys_3as"},
            { 34, "sys_30;sys_4as"},
            { 35, "sys_30;sys_5as"},
            { 36, "sys_30;sys_6as"},
            { 37, "sys_30;sys_7as"},
            { 38, "sys_30;sys_8as"},
            { 39, "sys_30;sys_9as"},

            { 40, "sys_40as"},
            { 41, "sys_40;sys_1as"},
            { 42, "sys_40;sys_2as"},
            { 43, "sys_40;sys_3as"},
            { 44, "sys_40;sys_4as"},
            { 45, "sys_40;sys_5as"},
            { 46, "sys_40;sys_6as"},
            { 47, "sys_40;sys_7as"},
            { 48, "sys_40;sys_8as"},
            { 49, "sys_40;sys_9as"},

            { 50, "sys_50as"},
            { 51, "sys_50;sys_1as"},
            { 52, "sys_50;sys_2as"},
            { 53, "sys_50;sys_3as"},
            { 54, "sys_50;sys_4as"},
            { 55, "sys_50;sys_5as"},
            { 56, "sys_50;sys_6as"},
            { 57, "sys_50;sys_7as"},
            { 58, "sys_50;sys_8as"},
            { 59, "sys_50;sys_9as"},

            { 60, "sys_60as"},
            { 61, "sys_60;sys_1as"},
            { 62, "sys_60;sys_2as"},
            { 63, "sys_60;sys_3as"},
            { 64, "sys_60;sys_4as"},
            { 65, "sys_60;sys_5as"},
            { 66, "sys_60;sys_6as"},
            { 67, "sys_60;sys_7as"},
            { 68, "sys_60;sys_8as"},
            { 69, "sys_60;sys_9as"},

            { 70, "sys_70as"},
            { 71, "sys_70;sys_1as"},
            { 72, "sys_70;sys_2as"},
            { 73, "sys_70;sys_3as"},
            { 74, "sys_70;sys_4as"},
            { 75, "sys_70;sys_5as"},
            { 76, "sys_70;sys_6as"},
            { 77, "sys_70;sys_7as"},
            { 78, "sys_70;sys_8as"},
            { 79, "sys_70;sys_9as"},

            { 80, "sys_80as"},
            { 81, "sys_80;sys_1as"},
            { 82, "sys_80;sys_2as"},
            { 83, "sys_80;sys_3as"},
            { 84, "sys_80;sys_4as"},
            { 85, "sys_80;sys_5as"},
            { 86, "sys_80;sys_6as"},
            { 87, "sys_80;sys_7as"},
            { 88, "sys_80;sys_8as"},
            { 89, "sys_80;sys_9as"},

            { 90, "sys_90as"},
            { 91, "sys_90;sys_1as"},
            { 92, "sys_90;sys_2as"},
            { 93, "sys_90;sys_3as"},
            { 94, "sys_90;sys_4as"},
            { 95, "sys_90;sys_5as"},
            { 96, "sys_90;sys_6as"},
            { 97, "sys_90;sys_7as"},
            { 98, "sys_90;sys_8as"},
            { 99, "sys_90;sys_9as"},

            { 100, "sys_100as"},
            { 101, "sys_100;sys_1as"},
            { 102, "sys_100;sys_2as"},
            { 103, "sys_100;sys_3as"},
            { 104, "sys_100;sys_4as"},
            { 105, "sys_100;sys_5as"},
            { 106, "sys_100;sys_6as"},
            { 107, "sys_100;sys_7as"},
            { 108, "sys_100;sys_8as"},
            { 109, "sys_100;sys_9as"},

            { 110, "sys_100;sys_10as"},
            { 111, "sys_100;sys_11as"},
            { 112, "sys_100;sys_12as"},
            { 113, "sys_100;sys_13as"},
            { 114, "sys_100;sys_14as"},
            { 115, "sys_100;sys_15as"},
            { 116, "sys_100;sys_60as"},
            { 117, "sys_100;sys_17as"},
            { 118, "sys_100;sys_18as"},
            { 119, "sys_100;sys_19as"},

            { 120, "sys_100;sys_20as"},
            { 121, "sys_100;sys_20;sys_1as"},
            { 122, "sys_100;sys_20;sys_2as"},
            { 123, "sys_100;sys_20;sys_3as"},
            { 124, "sys_100;sys_20;sys_4as"},
            { 125, "sys_100;sys_20;sys_5as"},
            { 126, "sys_100;sys_20;sys_6as"},
            { 127, "sys_100;sys_20;sys_7as"},
            { 128, "sys_100;sys_20;sys_8as"},
            { 129, "sys_100;sys_20;sys_9as"},

            { 130, "sys_100;sys_30as"},
            { 131, "sys_100;sys_30;sys_1as"},
            { 132, "sys_100;sys_30;sys_2as"},
            { 133, "sys_100;sys_30;sys_3as"},
            { 134, "sys_100;sys_30;sys_4as"},
            { 135, "sys_100;sys_30;sys_5as"},
            { 136, "sys_100;sys_30;sys_6as"},
            { 137, "sys_100;sys_30;sys_7as"},
            { 138, "sys_100;sys_30;sys_8as"},
            { 139, "sys_100;sys_30;sys_9as"},

            { 140, "sys_100;sys_40as"},
            { 141, "sys_100;sys_40;sys_1as"},
            { 142, "sys_100;sys_40;sys_2as"},
            { 143, "sys_100;sys_40;sys_3as"},
            { 144, "sys_100;sys_40;sys_4as"},
            { 145, "sys_100;sys_40;sys_5as"},
            { 146, "sys_100;sys_40;sys_6as"},
            { 147, "sys_100;sys_40;sys_7as"},
            { 148, "sys_100;sys_40;sys_8as"},
            { 149, "sys_100;sys_40;sys_9as"},

            { 150, "sys_100;sys_50as"},
            { 151, "sys_100;sys_50;sys_1as"},
            { 152, "sys_100;sys_50;sys_2as"},
            { 153, "sys_100;sys_50;sys_3as"},
            { 154, "sys_100;sys_50;sys_4as"},
            { 155, "sys_100;sys_50;sys_5as"},
            { 156, "sys_100;sys_50;sys_6as"},
            { 157, "sys_100;sys_50;sys_7as"},
            { 158, "sys_100;sys_50;sys_8as"},
            { 159, "sys_100;sys_50;sys_9as"},

            { 160, "sys_100;sys_60as"},
            { 161, "sys_100;sys_60;sys_1as"},
            { 162, "sys_100;sys_60;sys_2as"},
            { 163, "sys_100;sys_60;sys_3as"},
            { 164, "sys_100;sys_60;sys_4as"},
            { 165, "sys_100;sys_60;sys_5as"},
            { 166, "sys_100;sys_60;sys_6as"},
            { 167, "sys_100;sys_60;sys_7as"},
            { 168, "sys_100;sys_60;sys_8as"},
            { 169, "sys_100;sys_60;sys_9as"},

            { 170, "sys_100;sys_70as"},
            { 171, "sys_100;sys_70;sys_1as"},
            { 172, "sys_100;sys_70;sys_2as"},
            { 173, "sys_100;sys_70;sys_3as"},
            { 174, "sys_100;sys_70;sys_4as"},
            { 175, "sys_100;sys_70;sys_5as"},
            { 176, "sys_100;sys_70;sys_6as"},
            { 177, "sys_100;sys_70;sys_7as"},
            { 178, "sys_100;sys_70;sys_8as"},
            { 179, "sys_100;sys_70;sys_9as"},

            { 180, "sys_100;sys_80as"},
            { 181, "sys_100;sys_80;sys_1as"},
            { 182, "sys_100;sys_80;sys_2as"},
            { 183, "sys_100;sys_80;sys_3as"},
            { 184, "sys_100;sys_80;sys_4as"},
            { 185, "sys_100;sys_80;sys_5as"},
            { 186, "sys_100;sys_80;sys_6as"},
            { 187, "sys_100;sys_80;sys_7as"},
            { 188, "sys_100;sys_80;sys_8as"},
            { 189, "sys_100;sys_80;sys_9as"},

            { 190, "sys_100;sys_90as"},
            { 191, "sys_100;sys_90;sys_1as"},
            { 192, "sys_100;sys_90;sys_2as"},
            { 193, "sys_100;sys_90;sys_3as"},
            { 194, "sys_100;sys_90;sys_4as"},
            { 195, "sys_100;sys_90;sys_5as"},
            { 196, "sys_100;sys_90;sys_6as"},
            { 197, "sys_100;sys_90;sys_7as"},
            { 198, "sys_100;sys_90;sys_8as"},
            { 199, "sys_100;sys_90;sys_9as"},
        };
    

    

        public static string CreateFileName(string filename)
        {
            try
            {
                filename = filename.ToLower();

                filename = filename.Replace(" ", "");
                filename = filename.Replace(".", "");
                filename = filename.Replace(",", "");
                filename = filename.Replace("\"", "");
                filename = filename.Replace("'", "");
                filename = filename.Replace("„", "");
                filename = filename.Replace("“", "");
                filename = filename.Replace("-", "");


                filename = filename.Replace("ą", "a");
                filename = filename.Replace("č", "c");
                filename = filename.Replace("ę", "e");
                filename = filename.Replace("ė", "e");
                filename = filename.Replace("į", "i");
                filename = filename.Replace("š", "s");
                filename = filename.Replace("ų", "u");
                filename = filename.Replace("ū", "u");
                filename = filename.Replace("ž", "z");

                return filename;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
