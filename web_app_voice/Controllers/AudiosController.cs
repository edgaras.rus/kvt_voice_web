﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using web_app_voice.Extensions;
using web_app_voice.Models;
using web_app_voice.ModelsDto.AudiosDto;
using web_app_voice.Repositories.Interfaces;
using static web_app_voice.ModelsDto.DataTables.DatatableModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace web_app_voice.Controllers
{
    [Authorize]
    public class AudiosController : Controller
    {

        private readonly IAudiosRepository _audiosRepository;
        private readonly IOrganizationRepository _organizationRepository;
        private readonly UserManager<User> _userManager;

        public AudiosController(IAudiosRepository audiosRepository, IOrganizationRepository organizationRepository, UserManager<User> userManager)
        {
            _audiosRepository = audiosRepository;
            _organizationRepository = organizationRepository;
            _userManager = userManager;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewData["OrganizationID"] = GetUser().OrganizationID;

            return View();
        }

        public IActionResult LoadDataTable(DTParameters dtParameters, int? organizationID = null, int? audioType = null, int? audioFiles = null)
        {
            var searchBy = dtParameters.Search?.Value;

            var orderCriteria = string.Empty;
            var orderAscendingDirection = true;

            if (dtParameters.Order != null)
            {
                orderCriteria = dtParameters.Columns[dtParameters.Order[0].Column].Data;
                orderAscendingDirection = dtParameters.Order[0].Dir.ToString().ToLower() == "asc";
            }
            else
            {
                orderCriteria = "TagID";
                orderAscendingDirection = true;
            }

            if (audioType == 0)
                audioType = null;

            if (audioFiles == 0)
                audioFiles = null;

            var result = _audiosRepository.LoadAudioList(dtParameters, organizationID, audioType, audioFiles);

            var totalResultsCount = result.Count();

            /*

            if (!string.IsNullOrEmpty(searchBy))
            {
                 result = result.Where(r => r.SystemName != null
                    && r.SystemName.ToUpper().Contains(searchBy.ToUpper())).ToList();
            }
            */

            result = orderAscendingDirection ? result.AsQueryable().OrderByDynamic(orderCriteria, LinqExtensions.Order.Asc).ToList()
                : result.AsQueryable().OrderByDynamic(orderCriteria, LinqExtensions.Order.Desc).ToList();

            // now just get the count of items (without the skip and take) - eg how many could be returned with filtering
            var filteredResultsCount = result.Count();

            /*

            if (audioType.HasValue && audioType.Value != 0)
            {
                switch (audioType)
                {
                    case 1:
                        result = result.Where(x => x.AudioTypeID == 1).ToList();
                        break;

                    case 2:
                        result = result.Where(x => x.AudioTypeID == 2).ToList();
                        break;

                    case 3:
                        result = result.Where(x => x.AudioTypeID == 3).ToList();
                        break;

                    case 4:
                        result = result.Where(x => x.AudioTypeID == 4).ToList();
                        break;

                    default:
                        break;
 
                }
            }

            if (audioFiles.HasValue && audioFiles.Value != 0)
            {
                switch (audioFiles)
                {
                    case 1:
                        result = result.Where(x => x.AudioBase64 != null && x.AudioBase64.Length > 0).ToList();

                        break;

                    case 2:
                        result = result.Where(x => x.AudioBase64 == null || x.AudioBase64.Length == 0).ToList();
                        break;

                    default:
                        break;

                }
            }


            

    */
            result = result.Skip(dtParameters.Start)
                        .Take(dtParameters.Length)
                        .ToList();

            _audiosRepository.LoadAudioFiles(result);

            return Json(new
            {
                draw = dtParameters.Draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });

            /*
             * 
             *     .Skip(dtParameters.Start)
                    .Take(dtParameters.Length)

    */
        }

        [HttpPost]

        public IActionResult CreateEditModal(int? audioID)
        {
            try
            {
                AudioCreateEditDto audioCreateEditDto = new AudioCreateEditDto();
                if (audioID.HasValue)
                    audioCreateEditDto = _audiosRepository.LoadAudioDto(audioID.Value);

                audioCreateEditDto.Organizations = _organizationRepository.GetOrganizationsForDropDown(GetUser().OrganizationID);

                return PartialView("~/Views/Audios/Modals/_CreateEditModal.cshtml", audioCreateEditDto);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        [HttpPost]
        public IActionResult SaveModalData(AudioCreateEditDto audioCreateEditDto)
        {
            if (ModelState.IsValid)
            {
                _audiosRepository.SaveAudioCreateEditDto(audioCreateEditDto);
            }
            return PartialView("~/Views/Audios/Modals/_CreateEditModal.cshtml", audioCreateEditDto);
        }


        private User GetUser()
        {
            return _userManager.FindByIdAsync(User.FindFirstValue(ClaimTypes.NameIdentifier)).Result;
        }

     
    }
}
