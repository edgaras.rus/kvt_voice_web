﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Organizations
{

    [Table("OrganizationScanners", Schema = "organizations")]
    public class OrganizationScanner
    {
        [Key]
        public int OrganizationScannerID { get; set; }

        [Required]
        public string GUID { get; set; }

        [Required]
        public int OrganizationID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        public string Description { get; set; }

        public DateTime LastComunicationDate { get; set; }

        [ForeignKey("OrganizationID")]
        public virtual Organization Organization { get; set; }

        public OrganizationScanner()
        {
            this.LastComunicationDate = DateTime.MinValue;
        }

        public void Update()
        {
            this.LastComunicationDate = DateTime.Now;
        }

    }
}
