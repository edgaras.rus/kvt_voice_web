﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Models.Organizations;

namespace web_app_voice.Models.Tags
{
    [Table("TagLinks", Schema = "tags")]
    public class TagLink
    {
        public int TagLinkID { get; set; }

        public int OrganizationID { get; set; }

        public int TagLinkTypeID { get; set; }

        public int TagID { get; set; }

        public string ClientSystemID { get; set; }

        public DateTime? LastSeenDate { get; set; }

        public DateTime? LastPlanedDate { get; set; }

        [ForeignKey("OrganizationID")]
        public virtual Organization Organization { get; set; }


        [ForeignKey("TagLinkTypeID")]
        public virtual TagLinkType TagLinkType { get; set; }
    }
}
