﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Transports
{
    [Table("TransportTrips", Schema = "transports")]
    public class TransportTrip
    {
        [Key]
        public int TransportTripID { get; set; }

        [Required]
        public int DailyTransportTripID { get; set; }

        [Required]
        public string ClientSystemID { get; set; }

        public string RouteName { get; set; }

        public string DirectionID { get; set; }

        public string DirectionName { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        public string PlayList { get; set; }

        public string ShortPlayList { get; set; }

        public string ContentLithuania { get; set; }

        public string ShortContentLithuania { get; set; }

        public string ContentEnglish { get; set; }

        public string ShortContentEnglish { get; set; }

        [ForeignKey("DailyTransportTripID")]
        public virtual DailySchedule DailyTransportTrip { get; set; }

    }
}
