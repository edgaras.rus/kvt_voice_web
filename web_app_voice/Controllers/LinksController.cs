﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using web_app_voice.Extensions;
using web_app_voice.Models;
using web_app_voice.ModelsDto;
using web_app_voice.Repositories.Interfaces;
using static web_app_voice.ModelsDto.DataTables.DatatableModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace web_app_voice.Controllers
{
    [Authorize]
    public class LinksController : Controller
    {

        private readonly ILinksRepository _linksRepository;
        private readonly IOrganizationRepository _organizationRepository;
        private readonly UserManager<User> _userManager;

        public LinksController(ILinksRepository linksRepository, IOrganizationRepository organizationRepository, UserManager<User> userManager)
        {
            this._linksRepository = linksRepository;
            this._organizationRepository = organizationRepository;
            _userManager = userManager;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewData["OrganizationID"] = GetUser().OrganizationID;
            return View();
        }

        [HttpPost]

        public IActionResult CreateEditModal(int? linkID)
        {
            try
            {
                TagLinkCreateEditDto tagLinkCreateEditDto = new TagLinkCreateEditDto();
                if (linkID.HasValue)
                {
                    tagLinkCreateEditDto = _linksRepository.LoadTagLinkCreateEditDto(linkID.Value);
                }

                tagLinkCreateEditDto.Organizations = _organizationRepository
                    .GetOrganizationsForDropDown(GetUser().OrganizationID);

                return PartialView("~/Views/Links/Modals/_CreateEditLinkPartial.cshtml", tagLinkCreateEditDto);
            }
            catch(Exception e)
            {
                return null;
            }
        }


        [HttpPost]

        public IActionResult SaveModalData(TagLinkCreateEditDto tagLinkCreateEditDto)
        {
            if (ModelState.IsValid)
            {






                _linksRepository.SaveUpdate(tagLinkCreateEditDto);
            }
            return PartialView("~/Views/Links/Modals/_CreateEditLinkPartial.cshtml", tagLinkCreateEditDto);
        }


        public IActionResult LoadDataTable(DTParameters dtParameters, int? organizationID = null, int? linkTypeID = null, int? lastSeenTypeID = null)
        {
            var searchBy = dtParameters.Search?.Value;

            var orderCriteria = string.Empty;
            var orderAscendingDirection = true;

            if (dtParameters.Order != null)
            {
                orderCriteria = dtParameters.Columns[dtParameters.Order[0].Column].Data;
                orderAscendingDirection = dtParameters.Order[0].Dir.ToString().ToLower() == "asc";
            }
            else
            {
                orderCriteria = "TagID";
                orderAscendingDirection = true;
            }

            var result = _linksRepository.LoadTagLinks(organizationID);

            var totalResultsCount = result.Count();

            if (!string.IsNullOrEmpty(searchBy))
            {
                result = result.Where(r => r.ClientSystemID != null
                    && r.ClientSystemID.ToUpper().Contains(searchBy.ToUpper())).ToList();
            }

            if (linkTypeID.HasValue)
            {
                switch (linkTypeID)
                {
                    case 0:
                        break;

                    case 2:
                        result = result.Where(x => x.TagLinkID == 1).ToList();
                        break;

                    case 3:
                        result = result.Where(x => x.TagLinkID == 2).ToList();
                        break;

                    case 4:
                        result = result.Where(x => x.TagLinkID == 3).ToList();
                        break;

                    default:
                        break;

                }
            }

            if (lastSeenTypeID.HasValue)
            {
                switch (lastSeenTypeID)
                {
                    case 0:
                        break;

                    case 1:
                        result = result.Where(x => x.LastSeenDateTime.HasValue 
                            && x.LastSeenDateTime.Value > DateTime.Now.AddDays(-1)).ToList();
                        break;

                    case 2:
                        result = result.Where(x => x.LastSeenDateTime.HasValue 
                            && x.LastSeenDateTime.Value < DateTime.Now.AddDays(-1) 
                            &&  x.LastSeenDateTime.Value > DateTime.Now.AddDays(-5)).ToList();
                        break;

                    case 3:
                        result = result.Where(x => ! x.LastSeenDateTime.HasValue 
                            || ( x.LastSeenDateTime.HasValue && x.LastSeenDateTime.Value < DateTime.Now.AddDays(-5))).ToList();
                        break;

                    default:
                        break;

                }
            }

            result = orderAscendingDirection ? result.AsQueryable().OrderByDynamic(orderCriteria, LinqExtensions.Order.Asc).ToList()
                : result.AsQueryable().OrderByDynamic(orderCriteria, LinqExtensions.Order.Desc).ToList();

            // now just get the count of items (without the skip and take) - eg how many could be returned with filtering
            var filteredResultsCount = result.Count();

            return Json(new
            {
                draw = dtParameters.Draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
                    .Skip(dtParameters.Start)
                    .Take(dtParameters.Length)
                    .ToList()
            });
        }

        private User GetUser()
        {
            return _userManager.FindByIdAsync(User.FindFirstValue(ClaimTypes.NameIdentifier)).Result;
        }

      
    }

}
