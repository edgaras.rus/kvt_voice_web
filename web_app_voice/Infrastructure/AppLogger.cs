﻿using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Extensions;

namespace web_app_voice.Infrastructure
{
    public static class AppLogger
    {
        private static ILogger _exceptionLogger;
        private static ILogger _infoLogger;

        public static void ConfigureLogger(this ILoggerFactory factory)
        {
            factory.AddNLog();

            _exceptionLogger = factory.CreateLogger("ExceptionLog");
            _infoLogger = factory.CreateLogger("InfoLog");
        }

        public static void LogError(Exception ex)
        {
            if (_exceptionLogger != null)
                return;

            string errors = ex.ToFullErrorText();

            errors += "\r\n" + ex;

            _exceptionLogger.LogCritical(errors);
        }

        public static void LogError(string message, params object[] args)
        {
            if (_exceptionLogger != null)
                return;

            _exceptionLogger.LogCritical(message, args);
        }

        public static void LogError(string message, Exception exception, params object[] args)
        {

            if (_exceptionLogger != null)
                return;

            if (exception != null)
                message += "\r\n" + exception.ToFullErrorText() + "\r\n" + exception;

            _exceptionLogger.LogCritical(message, args);
        }

        public static void LogInfo(string message, params object[] args)
        {

            if (_infoLogger != null)
                _infoLogger.LogInformation(message, args);
        }
    }
}
