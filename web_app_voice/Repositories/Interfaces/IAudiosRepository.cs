﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Models.Audio;
using web_app_voice.ModelsDto.AndroidDto;
using web_app_voice.ModelsDto.AudiosDto;
using static web_app_voice.ModelsDto.DataTables.DatatableModels;

namespace web_app_voice.Repositories.Interfaces
{
    public interface IAudiosRepository
    {


        Audio GetAudioFileById(int audioID);

        Audio GetAudioFileSystemName(string systemName);

        List<AudioAndroid> GetAndroidAudioFilesList(DateTime dateTime);

        List<AudioDto> LoadAudioList(int? organizationID);

        AudioCreateEditDto LoadAudioDto(int? audioID);

        void SaveAudioCreateEditDto(AudioCreateEditDto audioCreateEditDto);

        void CreateAudioFile(Audio audio);

        void UpdateAudioFile(Audio audio);

        List<AudioDto> LoadAudioList(DTParameters dtParameters, int? organizationID = null, int? audioType = null, int? audioFiles = null);

        void LoadAudioFiles(List<AudioDto> audioDtos);
    }
}
