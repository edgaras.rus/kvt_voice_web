﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models
{
    public class User : IdentityUser
    {
        public int? OrganizationID { get; set; }

        public DateTime LastLoginTime { get; set; }

        [DefaultValue(false)]
        public bool IsBlocked { get; set; }

        [DefaultValue(false)]
        public bool IsActive { get; set; }
    }
}
