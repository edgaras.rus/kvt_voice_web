﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Audio
{
    public enum TagTypes
    {
        Bus = 1,

        Troley = 2,

        MiniBus = 3,

        Station = 4
    }
}
