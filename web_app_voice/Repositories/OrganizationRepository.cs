﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Data;
using web_app_voice.Models.Organizations;
using web_app_voice.Repositories.Interfaces;

namespace web_app_voice.Repositories
{
    public class OrganizationRepository : IOrganizationRepository
    {

        private readonly ApplicationDbContext _context;

        public OrganizationRepository(ApplicationDbContext applicationDbContext)
        {
            _context = applicationDbContext;
        }


        public List<Organization> LoadOrganizations()
        {
            return _context.Organizations.ToList();
        }

        public Organization LoadOrganization(int organizationID)
        {
            return _context.Organizations
                .Where(x => x.OrganizationID == organizationID)
                .FirstOrDefault();
        }


        public List<SelectListItem> GetOrganizationsForDropDown(int? organizationID)
        {
            if (!organizationID.HasValue)
            {
                return _context.Organizations.Select(o => new SelectListItem()
                {
                    Text = o.Name,
                    Value = o.OrganizationID.ToString()
                }).ToList();
            }
            else
            {
                return _context.Organizations
                    .Where(o => o.OrganizationID == organizationID)
                    .Select(o => new SelectListItem()
                    {
                        Text = o.Name,
                        Value = o.OrganizationID.ToString()
                    }).ToList();
            }
        }

    }
}
