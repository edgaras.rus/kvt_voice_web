﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Models.Enums;

namespace web_app_voice.ModelsDto.TagsDto
{
    public class TagDto
    {
        public int Id { get; set; }
        public string Mac { get; set; }
        public string NameSpce { get; set; }
        public string Instance { get; set; }
        public string Url { get; set; }
        public string TagData { get; set; }

        public string StationName { get; set; }

        public string RouteName { get; set; }

        public int Type { get; set; }
        public DateTime LastSeenDate { get; set; }

        public TransportTypes TransportType { get; set; }

        public TagStateTypes TransportState { get; set; }

        public virtual List<TagDetailDto> TagDetails { get; set; }

        public bool IsRegistered { get; set; }
    }
}
