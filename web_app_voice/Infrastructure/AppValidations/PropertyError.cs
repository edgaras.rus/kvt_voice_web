﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Infrastructure.AppValidations
{
    public class PropertyError
    {
        public string ColumnName { get; set; }
        public string ErrorMessage { get; set; }

        public PropertyError() { }

        public PropertyError(string errorMessage)
        {

            ErrorMessage = errorMessage;
        }

        public PropertyError(string columnName, string errorMessage)
        {
            ColumnName = char.ToLower(columnName[0]) + columnName.Substring(1);
            ErrorMessage = errorMessage;
        }
    }
}
