﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.TagsDto
{
    public class TagChartItemDto
    {
        public DateTime Time { get; set; }

        public int RSSI { get; set; }
    }
}
