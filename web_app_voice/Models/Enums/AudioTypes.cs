﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Enums
{
    public enum AudioTypes
    {
        System = 1,

        Transport = 2,

        Station = 3
    }
}
