﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Infrastructure.AppValidations
{
    public static class ValidationKeywords
    {
        public const string Required = "validRequired";
        public const string MaxLength = "validLength";
        public const string StringLength = "validStringLength";
        public const string EmailAddress = "validEmailAddress";
        public const string Phone = "validPhoneNo";
    }
}
