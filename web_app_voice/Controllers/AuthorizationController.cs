﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Infrastructure;
using web_app_voice.Infrastructure.AppValidations;
using web_app_voice.Models;
using web_app_voice.ModelsDto;

namespace web_app_voice.Controllers
{
    [AllowAnonymous]
    public class AuthorizationController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IPasswordHasher<User> _passwordHasher;


        private const string _notValidAuthInputs = "Incorrect username or password";

        public AuthorizationController(UserManager<User> userManager, SignInManager<User> signInManager,
            IPasswordHasher<User> passwordHasher)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _passwordHasher = passwordHasher;
        }

        #region Login 
        //--------------------------------------------------------------------------------------------------

        [HttpGet]
        public IActionResult Login(string returnUrl)
        {
            returnUrl = Url.Content(returnUrl) ?? Url.Content("/");

            return View(new LoginDataDto() { ReturnUrl = returnUrl });
        }

        //--------------------------------------------------------------------------------------------------
        #endregion

        [HttpPost]
        public async Task<IActionResult> PerFormLogin(LoginDataDto loginDataDto)
        {
            var result = ModelState.GetErrors(true);

            if (result.HasError)
                return result.JsonResult();

            try
            {
                var user = await _userManager.FindByNameAsync(loginDataDto.LoginEmail);

                if (user == null)
                    return new ValidationResult(new PropertyError(_notValidAuthInputs)).JsonResult();

                var validPassword = _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, loginDataDto.LoginPassword);
                if (validPassword != PasswordVerificationResult.Success)
                {
                    await UpdateLoginInfo(user, true);
                    return new ValidationResult(new PropertyError(_notValidAuthInputs)).JsonResult();
                }

                if (user.IsBlocked)
                    return new ValidationResult(new PropertyError("Account is blocked")).JsonResult();
                else if (!user.IsActive)
                    return new ValidationResult(new PropertyError("Account is inactive")).JsonResult();

                var signInResult = await _signInManager.PasswordSignInAsync(loginDataDto.LoginEmail, loginDataDto.LoginPassword, false, lockoutOnFailure: true);
             

                if (signInResult.Succeeded)
                {
                  //  await _appClaimsTransformation.ForceTransformAsync(User);
                    await UpdateLoginInfo(user, false);

                    return Json(loginDataDto.ReturnUrl);
                }
                else if (signInResult.IsLockedOut)
                {
                    await UpdateLoginInfo(user, true);
                    return new ValidationResult(new PropertyError("User account is blocked")).JsonResult();
                }
                else if (signInResult.IsNotAllowed)
                {
                    await UpdateLoginInfo(user, true);
                    return new ValidationResult(new PropertyError("User account access is denied")).JsonResult();
                }
                else
                {
                    string error = $"Login failed. SignInManager failed sign. User='{loginDataDto.LoginEmail}'. SignInResult.Succeeded='{signInResult.Succeeded}';";
                    error += $"SignInResult.IsLockedOut='{signInResult.IsLockedOut}; SignInResult.IsNotAllowed='{signInResult.IsNotAllowed}''";
                    AppLogger.LogError(error);

                    await UpdateLoginInfo(user, true);

                    return new ValidationResult(new PropertyError("Login failed")).JsonResult();
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogError("Login failed. Critical error.\r\n" + ex);

                return new ValidationResult(new PropertyError("Login failed")).JsonResult();
            }
        }

        private async Task UpdateLoginInfo(User user, bool isError)
        {
            try
            {
                if (!isError)
                {
                    user.LastLoginTime = DateTime.Now;
                    await _userManager.UpdateAsync(user);
                }
            }
            catch (Exception ex)
            {
                AppLogger.LogError($"Failed update user data on login. User='{user.Email}'; OnError='{isError}';\r\n" + ex);
            }
        }
        //--------------------------------------------------------------------------------------------------

        #region Logout
        //--------------------------------------------------------------------------------------------------
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();

            return RedirectToAction("Login", "Authorization");
        }
        //--------------------------------------------------------------------------------------------------
        #endregion

        public IActionResult AccessDenied()
        {
            return RedirectToAction("Index", "Home");
        }
    }
}
