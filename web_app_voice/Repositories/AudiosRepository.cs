﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Data;
using web_app_voice.Models.Audio;
using web_app_voice.ModelsDto.AndroidDto;
using web_app_voice.ModelsDto.AudiosDto;
using web_app_voice.Repositories.Interfaces;
using static web_app_voice.ModelsDto.DataTables.DatatableModels;

namespace web_app_voice.Repositories
{
    public class AudiosRepository : IAudiosRepository
    {
        private readonly ApplicationDbContext _context;

        public AudiosRepository(ApplicationDbContext applicationDbContext)
        {
            _context = applicationDbContext;
        }

        public List<AudioDto> LoadAudioList(int? organizationID)
        {
            if (organizationID.HasValue)
            {
                return _context.Audios
                    .Where(a => a.OrganizationID == organizationID.Value)
                    .Include(a => a.Organization)
                    .Include(a => a.AudioType)
                    .Select(a => new AudioDto
                    {
                        AudioID = a.AudioID,
                        OrganizationID = a.OrganizationID,
                        OrganizationName = a.Organization.Name,
                        AudioTypeID = a.AudioTypeID,
                        AudioTypeName = a.AudioType.Name,
                        SystemName = a.SystemName,
                        SpeakOutLTU = a.SpeakOutLtu,
                        SpeakOutEN = a.SpeakOutEn,
                        Active = a.Active,
                        Bytes = a.Bytes,
                        AudioBase64 = a.Bytes != null ? Convert.ToBase64String(a.Bytes) : "",
                        UpdateDate = a.UpdateDate

                    })
                    .ToList();
            }

            return _context.Audios
                    .Include(a => a.Organization)
                    .Include(a => a.AudioType)
                    .Select(a => new AudioDto
                    {
                        AudioID = a.AudioID,
                        OrganizationID = a.OrganizationID,
                        OrganizationName = a.Organization.Name,
                        AudioTypeID = a.AudioTypeID,
                        AudioTypeName = a.AudioType.Name,
                        SystemName = a.SystemName,
                        SpeakOutLTU = a.SpeakOutLtu,
                        SpeakOutEN = a.SpeakOutEn,
                        Active = a.Active,
                        Bytes = a.Bytes,
                        AudioBase64 = a.Bytes != null ? Convert.ToBase64String(a.Bytes) : "",
                        UpdateDate = a.UpdateDate
                    })
                    .ToList();
        }
        public AudioCreateEditDto LoadAudioDto(int? audioID)
        {
            if (audioID.HasValue)
                return _context.Audios
                    .Where(a => a.AudioID == audioID.Value)
                    .Include(a => a.Organization)
                    .Include(a => a.AudioType)
                    .Select(a => new AudioCreateEditDto
                    {
                        AudioID = a.AudioID,
                        OrganizationID = a.OrganizationID,
                        OrganizationName = a.Organization.Name,
                        AudioTypeID = a.AudioTypeID,
                        AudioTypeName = a.AudioType.Name,
                        SystemName = a.SystemName,
                        SpeakOutLTU = a.SpeakOutLtu,
                        SpeakOutEN = a.SpeakOutEn,
                        Active = a.Active,
                        Bytes = a.Bytes
                    })
                    .FirstOrDefault();

            return new AudioCreateEditDto();
        }

        public void SaveAudioCreateEditDto(AudioCreateEditDto audioCreateEditDto)
        {
            if (audioCreateEditDto.AudioID > 0)
            {
                Audio audio = _context.Audios.Where(a => a.AudioID == audioCreateEditDto.AudioID).FirstOrDefault();
                audio.OrganizationID = audioCreateEditDto.OrganizationID;
                audio.AudioTypeID = audioCreateEditDto.AudioTypeID;
                audio.SystemName = audioCreateEditDto.SystemName;
                audio.SpeakOutEn = audioCreateEditDto.SpeakOutEN;
                audio.SpeakOutLtu = audioCreateEditDto.SpeakOutLTU;
                audio.Active = audioCreateEditDto.Active;

                if (audioCreateEditDto.FileBase64 != null && audioCreateEditDto.FileBase64.Length > 0)
                {
                    audioCreateEditDto.FileBase64 = audioCreateEditDto.FileBase64.Replace(" ", "+");
                    audio.Bytes = Convert.FromBase64String(audioCreateEditDto.FileBase64);

                }


                audio.UpdateDate = DateTime.Now;

                _context.Audios.Update(audio);
                _context.SaveChanges();
            }
            else
            {
                Audio audio = new Audio()
                {
                    OrganizationID = audioCreateEditDto.OrganizationID,
                    AudioTypeID = audioCreateEditDto.AudioTypeID,
                    SystemName = audioCreateEditDto.SystemName,
                    SpeakOutLtu = audioCreateEditDto.SpeakOutLTU,
                    SpeakOutEn = audioCreateEditDto.SpeakOutEN,
                    Active = audioCreateEditDto.Active,
                    Bytes = audioCreateEditDto.Bytes,
                };

                _context.Audios.Add(audio);
                _context.SaveChanges();
            }
        }

        public void CreateAudioFile(Audio audio)
        {
            _context.Audios.Add(audio);
            _context.SaveChanges();
        }

        public void UpdateAudioFile(Audio audio)
        {
            _context.Audios.Update(audio);
            _context.SaveChanges();
        }

        public List<AudioAndroid> GetAndroidAudioFilesList(DateTime dateTime)
        {
            List<AudioAndroid> result = new List<AudioAndroid>();

            List<Audio> audios = _context.Audios
                .Where(a => a.Active && a.UpdateDate > dateTime)
                .ToList();

            foreach (Audio audio in audios)
            {
                if (audio == null)
                    continue;

                AudioAndroid audioAndroid = new AudioAndroid
                {
                    Id = audio.AudioID,
                    OrganizationId = audio.OrganizationID,
                    SystemName = audio.SystemName,
                    SpeakOutLithuania = audio.SpeakOutLtu,
                    SpeakOutEnglish = audio.SpeakOutEn,
                    HasFile = audio.Bytes != null && audio.Bytes.Length > 0 ? true : false,
                    UpdateDate = audio.UpdateDate.ToString("yyyy-MM-dd HH:mm:ss")
                };

                if (audioAndroid != null)
                    result.Add(audioAndroid);

            }
            return result;
        }

        public Audio GetAudioFileById(int audioID)
        {
            return _context.Audios.Where(x => x.AudioID == audioID).FirstOrDefault();
        }

        public Audio GetAudioFileSystemName(string systemName)
        {
            return _context.Audios.Where(x => x.SystemName.Equals(systemName)).FirstOrDefault();
        }


        public List<AudioDto> LoadAudioList(DTParameters dtParameters, int? organizationID = null, int? audioType = null, int? audioFiles = null)
        {
            var searchBy = dtParameters.Search?.Value;


            return _context.Audios
               .Where(a => (organizationID.HasValue  ?  a.OrganizationID == organizationID.Value :  a.OrganizationID == a.OrganizationID))
               .Where(a => !string.IsNullOrEmpty(searchBy) ?  a.SystemName != null
                    && a.SystemName.ToUpper().Contains(searchBy.ToUpper()) : a.SystemName == a.SystemName )
               .Where(a => audioType.HasValue ? a.AudioTypeID == audioType.Value : a.AudioTypeID == a.AudioTypeID)
               .Where(a => audioFiles.HasValue ? audioFiles.Value == 1 ? a.Bytes != null : a.Bytes == null : a.Bytes == a.Bytes)
               .Include(a => a.Organization)
               .Include(a => a.AudioType)
               .Select(a => new AudioDto
               {
                   AudioID = a.AudioID,
                   OrganizationID = a.OrganizationID,
                   OrganizationName = a.Organization.Name,
                   AudioTypeID = a.AudioTypeID,
                   AudioTypeName = a.AudioType.Name,
                   SystemName = a.SystemName,
                   SpeakOutLTU = a.SpeakOutLtu,
                   SpeakOutEN = a.SpeakOutEn,
                   Active = a.Active,
                   //Bytes = a.Bytes,
                   //AudioBase64 = a.Bytes != null ? Convert.ToBase64String(a.Bytes) : "",
                   UpdateDate = a.UpdateDate

               }).ToList();
        }

        public void LoadAudioFiles(List<AudioDto> audioDtos)
        {
            foreach(var audioDto in audioDtos)
            {
                var audio = _context.Audios.FirstOrDefault(a => a.AudioID == audioDto.AudioID);

                if(audio != null)
                {
                    audioDto.Bytes = audio.Bytes;
                    audioDto.AudioBase64 = audio.Bytes != null ? Convert.ToBase64String(audio.Bytes) : "";
                }
            }
        }
    }
}
