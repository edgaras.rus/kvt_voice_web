﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Models.Enums;

namespace web_app_voice.ModelsDto.TagsDto
{
    public class TagsFilterDto
    {
        public List<int> TransportTypes { get; set; }
        public List<int> StateValues { get; set; }
        public int TransportSortType { get; set; }
        public int SortType { get; set; }
        public string Search { get; set; }

        public TagsFilterDto()
        {
            this.StateValues = new List<int> { };
            this.TransportTypes = new List<int> { };
            this.TransportSortType = (int)TransportSort.Name;
            this.SortType = (int)SortTypes.Asc;
            this.Search = String.Empty;
        }

        public void SetDefaultFilter()
        {
            this.StateValues = new List<int> { (int)TagStateTypes.Okey, (int)TagStateTypes.Warrning, (int)TagStateTypes.Error };
            this.TransportTypes = new List<int> { (int)Models.Enums.TransportTypes.Bus, (int)Models.Enums.TransportTypes.Trolley, (int)Models.Enums.TransportTypes.Station };
        }
    }
}
