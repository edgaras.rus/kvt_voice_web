﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.SensorDto
{
    public class SensorEvent
    {
        public DateTime EventDate { get; set; }

        public int Type { get; set; }

        public string Description { get; set; }
    }
}
