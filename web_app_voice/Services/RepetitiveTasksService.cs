﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using web_app_voice.Data;
using web_app_voice.Infrastructure;
using web_app_voice.Models.Audio;
using web_app_voice.Models.Enums;
using web_app_voice.Models.Organizations;
using web_app_voice.Models.Tags;
using web_app_voice.Models.Transports;
using web_app_voice.Repositories.Interfaces;
using web_app_voice.Utilities;

namespace web_app_voice.Services
{
    public class RepetitiveTasksService : HostedService
    {
        private BackgroundWorker _worker;

        private readonly IRepetitiveTaskRepository _repetitiveTaskRepository;
        private readonly IOrganizationRepository _organizationRepository;
        private readonly ISchedulesRepository _schedulesRepository;
        private readonly IAudiosRepository _audiosRepository;
        private readonly ILinksRepository _linksRepository;
        private readonly IWebHostEnvironment _hostingEnvironment;


        public RepetitiveTasksService(IRepetitiveTaskRepository repetitiveTaskRepository, IOrganizationRepository organizationRepository,
            ISchedulesRepository schedulesRepository, IAudiosRepository audiosRepository, IWebHostEnvironment hostingEnvironment,
            ILinksRepository linksRepository)
        {
            _repetitiveTaskRepository = repetitiveTaskRepository;
            _organizationRepository = organizationRepository;
            _schedulesRepository = schedulesRepository;
            _audiosRepository = audiosRepository;
            _hostingEnvironment = hostingEnvironment;
            _linksRepository = linksRepository;
        }


        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {

                AppLogger.LogInfo($"Heart beat worker status {(_worker != null && _worker.IsBusy ? "Busy" : "Sleep")}");

                if (_worker == null || !_worker.IsBusy)
                {
                    _worker = new BackgroundWorker();
                    _worker.DoWork += _dbWorker_DoWork;
                    _worker.ProgressChanged += _dbWorker_ProgressChanged;
                    _worker.WorkerSupportsCancellation = true;
                    _worker.WorkerReportsProgress = true;
                    _worker.RunWorkerCompleted += _dbWorker_RunWorkerCompleted;
                    _worker.RunWorkerAsync();
                }

                await Task.Delay(TimeSpan.FromSeconds(60), cancellationToken);
            }
        }

        private void _dbWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void _dbWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void _dbWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                var repetitiveTasks = _repetitiveTaskRepository.LoadRepetitiveTasks();

                foreach (var repetitiveTask in repetitiveTasks)
                {
                    if (repetitiveTask.NexExecuteTime > DateTime.Now)
                        continue;

                    var watch = System.Diagnostics.Stopwatch.StartNew();

                    switch (repetitiveTask.RepetitiveTaskTypeID)
                    {
                        case 1:

                            AppLogger.LogInfo("Starting  update transport schedules..!");
                            UpdateTransportSchedules();
                            break;

                        case 2:

                            AppLogger.LogInfo("Starting update stations..!");
                            UpdateStations();

                            break;
                        case 3:

                            AppLogger.LogInfo("Starting check daily schedules audio..!");
                            CheckDailySchedulesAudio();
                            break;
                        case 4:

                            AppLogger.LogInfo("Starting eat audio files..!");
                            EatAudioFiles();
                            break;
                        case 5:

                            AppLogger.LogInfo("Starting recalcualte last planed..!");
                            RecalcualteLastPlaned();
                            break;
                        default:
                            break;
                    }

                    watch.Stop();

                    repetitiveTask.LastExecuteTime = DateTime.Now;
                    repetitiveTask.ElapsedMilliseconds = watch.ElapsedMilliseconds;
                    repetitiveTask.NexExecuteTime = DateTime.Now.AddSeconds(repetitiveTask.Frequency);

                    _repetitiveTaskRepository.UpdateRepetitiveTask(repetitiveTask);
                }
            }
            catch (Exception workerException)
            {
                AppLogger.LogError(workerException);
            }

        }

        #region TransportSchedules
        //--------------------------------------------------------------------------------------------------------------------//
        public void UpdateTransportSchedules()
        {
            try
            {
                var organizations = _organizationRepository.LoadOrganizations();

                List<Task<DailySchedule>> tasks = new List<Task<DailySchedule>>();
                foreach (var organization in organizations)
                {
                    Task<DailySchedule> t = new Task<DailySchedule>(() => LoadDailySchedule(organization));
                    t.Start();
                    tasks.Add(t);
                }

                Task.WaitAll(tasks.ToArray());

                foreach (var task in tasks)
                {
                    DailySchedule dailySchedule = task.Result;

                    DailySchedule _dailySchedule = _schedulesRepository.LoadDailyScheduleByOrganizationAndDeclare(dailySchedule.OrganizationID, dailySchedule.DeclareDate);

                    dailySchedule.Added = dailySchedule.TransportTrips.Count;

                    if (_dailySchedule != null)
                    {
                        List<TransportTrip> OldTransportTripData = _schedulesRepository.TransportTripsBySchedule(_dailySchedule.DailyTransportTripID);
                        List<TransportTrip> NewTransportTripData = dailySchedule.TransportTrips;

                        TransportTripDataComparer ttdC = new TransportTripDataComparer();

                        List<TransportTrip> NewTransportTripDataToAdd = new List<TransportTrip>();
                        NewTransportTripDataToAdd = NewTransportTripData.Except(OldTransportTripData, ttdC).ToList();

                        List<TransportTrip> NewTransportTripDataToRemove = new List<TransportTrip>();
                        NewTransportTripDataToRemove = OldTransportTripData.Except(NewTransportTripData, ttdC).ToList();

                        _dailySchedule.Eddited = _dailySchedule.Eddited + NewTransportTripDataToAdd.Count;
                        _dailySchedule.Removed = _dailySchedule.Removed + NewTransportTripDataToRemove.Count;


                        _schedulesRepository.SaveDailySchedule(_dailySchedule);
                    }

                    _schedulesRepository.SaveDailySchedule(dailySchedule);

                }
            }
            catch (Exception e)
            {
                AppLogger.LogError("Update Transport Schedules", e);
            }
        }

        public void RecalcualteLastPlaned()
        {
            try
            {
                var organizations = _organizationRepository.LoadOrganizations();

                List<TagLink> tagLinksToUpdate = 
                    new List<TagLink>();


                foreach(var organization in organizations)
                {
                    var schedule =  _schedulesRepository.LoadLastOrganizationDailySchedule(organization.OrganizationID);

                    var transportLinks = _linksRepository.GetTagLinks()
                            .Where(x => x.TagLinkTypeID == 1 || x.TagLinkTypeID == 2 && x.OrganizationID == organization.OrganizationID)
                            .ToList();

                    foreach(var transportLink in transportLinks)
                    {
                        if (schedule.TransportTrips.Any(tt => tt.ClientSystemID == transportLink.ClientSystemID))
                        {
                            if (transportLink.LastPlanedDate.HasValue)
                            {
                                if (transportLink.LastPlanedDate < schedule.DeclareDate)
                                {
                                    transportLink.LastPlanedDate = schedule.DeclareDate;
                                    _linksRepository.UpdateTagLink(transportLink);
                                }
                            }
                            else
                            {
                                transportLink.LastPlanedDate = schedule.DeclareDate;
                                _linksRepository.UpdateTagLink(transportLink);
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                AppLogger.LogError("Recalcualte last planed..!", e);
            }
        }

        public DailySchedule LoadDailySchedule(Organization organization)
        {
            DailySchedule result = new DailySchedule();
            try
            {
                string[] lines = new HttpClient()
                    .GetStringAsync(organization.TripsDataUrl).Result
                    .Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);


                DateTime declareDate = DateTime.Parse(lines[lines.Length - 1]);

                // Bus turas work around, becouse they do not have declare date.
                if (organization.OrganizationID == 2)
                    declareDate = DateTime.Now;

                DailySchedule dailySchedule = new DailySchedule()
                {
                    DeclareDate = new DateTime(declareDate.Year, declareDate.Month, declareDate.Day, 0, 0, 0),
                    OrganizationID = organization.OrganizationID,
                    UpdateDate = DateTime.Now
                };

                for (int i = 1; i < lines.Length - 1; i++)
                {
                    string[] data = lines[i].Split(',');

                    if (data.Length < 11)
                        continue;

                    if (data[4] == null || data[1] == null || data[8] == null || data[10] == null)
                        continue;
                    try
                    {
                        DateTime _endDate = AppOps.RouteTimeFromWebToDateTime(data[7], declareDate);

                        if (DateTime.Now > _endDate)
                            continue;

                        dailySchedule.TransportTrips.Add(new TransportTrip()
                        {
                            DailyTransportTripID = dailySchedule.DailyTransportTripID,
                            ClientSystemID = data[4],
                            RouteName = data[1],
                            DirectionID = data[8],
                            DirectionName = data[10],
                            StartDate = AppOps.RouteTimeFromWebToDateTime(data[6], declareDate),
                            EndDate = AppOps.RouteTimeFromWebToDateTime(data[7], declareDate),

                            ContentLithuania = organization.OrganizationID == 3 ?
                                AppOps.GetRouteContentLithuanianByType(data[1], data[10],
                                        data[0].Equals("Autobusai")
                                        ? TransportTypes.Bus : data[0].Equals("Troleibusai")
                                        ? TransportTypes.Trolley
                                        : TransportTypes.Other)

                                : AppOps.GetRouteContentLithuanian(data[1], data[10]),
                            ShortContentLithuania = organization.OrganizationID == 3 ?
                            AppOps.GetShortContentLithuaniaByType(data[1],
                                        data[0].Equals("Autobusai")
                                        ? TransportTypes.Bus : data[0].Equals("Troleibusai")
                                        ? TransportTypes.Trolley
                                        : TransportTypes.Other)

                                : AppOps.GetShortContentLithuania(data[1]),
                            ContentEnglish = organization.OrganizationID == 3 ?
                                AppOps.GetConetntEnglishByType(data[1], data[10],
                                        data[0].Equals("Autobusai")
                                        ? TransportTypes.Bus : data[0].Equals("Troleibusai")
                                        ? TransportTypes.Trolley
                                        : TransportTypes.Other)
                                : AppOps.GetConetntEnglish(data[1], data[10]),

                            ShortContentEnglish = organization.OrganizationID == 3 ?
                                AppOps.GetShortContentEnglishByType(data[1],
                                        data[0].Equals("Autobusai")
                                        ? TransportTypes.Bus : data[0].Equals("Troleibusai")
                                        ? TransportTypes.Trolley
                                        : TransportTypes.Other)

                                : AppOps.GetShortContentEnglish(data[1]),

                            PlayList = organization.OrganizationID == 3 ?
                                AppOps.GetTransportPlayListByType
                                    (
                                        data[1], data[10], data[0].Equals("Autobusai")
                                        ? TransportTypes.Bus : data[0].Equals("Troleibusai")
                                        ? TransportTypes.Trolley
                                        : TransportTypes.Other
                                    )
                                : AppOps.GetPlayList(data[1], data[10]),

                            ShortPlayList = organization.OrganizationID == 3 ?
                                AppOps.GetTransportShortPlayListByType
                                    (
                                        data[1], data[0].Equals("Autobusai")
                                        ? TransportTypes.Bus : data[0].Equals("Troleibusai")
                                        ? TransportTypes.Trolley
                                        : TransportTypes.Other
                                    )

                                : AppOps.GetShortPlayList(data[1], data[10]),
                        });
                    }
                    catch (Exception e)
                    {
                        AppLogger.LogError("Single load daily schedule", e, lines[i]);
                    }
                }

                result = dailySchedule;
            }
            catch (Exception e)
            {
                AppLogger.LogError("Load daily schedule", e);
            }

            return result;
        }

        //--------------------------------------------------------------------------------------------------------------------//
        #endregion

        #region Stations
        //--------------------------------------------------------------------------------------------------------------------//

        public void UpdateStations()
        {
            try
            {
                var organizations = _organizationRepository.LoadOrganizations();

                List<Station> stations = _schedulesRepository.LoadStations();

                List<Station> newStations = new List<Station>();
                List<Station> updateStations = new List<Station>();
                List<Audio> audioFiles = new List<Audio>();



                foreach (var organization in organizations)
                {
                    try
                    {
                        var httpClient = new HttpClient();
                        var result = httpClient.GetAsync(organization.StationsDataUrl).Result;
                        var stream = result.Content.ReadAsStreamAsync().Result;

                        XmlDocument doc1 = new XmlDocument();
                        doc1.Load(stream);
                        XmlElement root = doc1.DocumentElement;
                        XmlNodeList nodes = root.ChildNodes;

                        foreach (XmlNode node in nodes)
                        {
                            string _clientSystemID = organization.OrganizationID == 2 ? node.Attributes["id0"].Value : node.Attributes["id"].Value;

                            var _station = stations
                                .Where(x => x.ClientSystemID.Equals(_clientSystemID) && x.OrganizationID == organization.OrganizationID)
                                .FirstOrDefault();

                            if (_station != null)
                            {
                                var update = new Station()
                                {
                                    ClientSystemID = _clientSystemID,
                                    OrganizationID = organization.OrganizationID,
                                    StationName = organization.OrganizationID == 2 ? node.Attributes["name"].Value.Replace("\"", "") : node.Attributes["name"].Value,
                                    Latitude = Double.Parse(node.Attributes["lat"].Value),
                                    Longitude = Double.Parse(node.Attributes["lon"].Value),
                                    ContentLithunian = node.Attributes["name"].Value,
                                    ContentEnglish = node.Attributes["name"].Value,
                                    ShortContentLithunian = node.Attributes["name"].Value,
                                    ShortContentEnglish = node.Attributes["name"].Value,
                                    PlayList = "st_" + AppOps.CreateFileName(node.Attributes["name"].Value),
                                    ShortPlayList = "st_" + AppOps.CreateFileName(node.Attributes["name"].Value),
                                };


                                if (organization.OrganizationID == 3)
                                {
                                    update.StationDirection = node.Attributes["stopDirection"].Value;
                                    update.PlayList = update.PlayList + ";dr_" + AppOps.CreateFileName(update.StationDirection);
                                    update.ShortPlayList = update.ShortPlayList + ";dr_" + AppOps.CreateFileName(update.StationDirection);
                                }

                                if (_station.StationName != update.StationName || _station.Latitude != update.Latitude || _station.Longitude != update.Longitude ||
                                   _station.PlayList != update.PlayList || _station.ContentLithunian != update.ContentLithunian || _station.ContentEnglish != update.ContentEnglish)
                                {
                                    _station.StationName = update.StationName;
                                    _station.Latitude = update.Latitude;
                                    _station.Longitude = update.Longitude;
                                    _station.PlayList = update.PlayList;
                                    _station.ShortPlayList = update.ShortPlayList;
                                    _station.ShortContentLithunian = update.ShortContentLithunian;
                                    _station.ContentLithunian = update.ContentLithunian;
                                    _station.ContentEnglish = update.ContentEnglish;
                                    _station.ShortContentEnglish = update.ShortContentEnglish;

                                    updateStations.Add(_station);
                                }
                            }
                            else
                            {
                                Station _newStation = new Station()
                                {
                                    ClientSystemID = _clientSystemID,
                                    OrganizationID = organization.OrganizationID,
                                    StationName = organization.OrganizationID == 2 ? node.Attributes["name"].Value.Replace("\"", "") : node.Attributes["name"].Value,
                                    Latitude = Double.Parse(node.Attributes["lat"].Value),
                                    Longitude = Double.Parse(node.Attributes["lon"].Value),
                                    ContentLithunian = node.Attributes["name"].Value,
                                    ContentEnglish = node.Attributes["name"].Value,
                                    ShortContentLithunian = node.Attributes["name"].Value,
                                    ShortContentEnglish = node.Attributes["name"].Value,
                                    PlayList = "st_" + AppOps.CreateFileName(node.Attributes["name"].Value),
                                    ShortPlayList = "st_" + AppOps.CreateFileName(node.Attributes["name"].Value)
                                };

                                CreateAudioFile(_newStation.StationName, "st", 3, organization.OrganizationID);


                                if (organization.OrganizationID == 3)
                                {
                                    _newStation.StationDirection = node.Attributes["stopDirection"].Value;
                                    _newStation.PlayList = _newStation.PlayList + ";dr_" + AppOps.CreateFileName(_newStation.StationDirection);
                                    _newStation.ShortPlayList = _newStation.ShortPlayList + ";dr_" + AppOps.CreateFileName(_newStation.StationDirection);

                                    CreateAudioFile(_newStation.StationDirection, "dr", 4, organization.OrganizationID);
                                }

                                newStations.Add(_newStation);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        AppLogger.LogError("single organization update stations schedule..!", e);
                    }
                }
                _schedulesRepository.SaveStations(newStations);
                _schedulesRepository.UpdateStations(updateStations);
            }
            catch (Exception e)
            {
                AppLogger.LogError("Update stations..!", e);
            }
        }
        //--------------------------------------------------------------------------------------------------------------------//
        #endregion


        #region AudioFiles
        //--------------------------------------------------------------------------------------------------------------------//

        private void CheckDailySchedulesAudio()
        {
            DateTime declareDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

            try
            {
                var dailySchedules = _schedulesRepository.LoadDailySchedulesByDeclare(declareDate);

                foreach (var dailySchedule in dailySchedules)
                {
                    foreach (var transportTrip in dailySchedule.TransportTrips)
                    {
                        try
                        {
                            if (transportTrip.DirectionName != null && transportTrip.DirectionName.Length > 0)
                                CreateAudioFile(transportTrip.DirectionName, "tr", 2, dailySchedule.OrganizationID);
                        }
                        catch (Exception e)
                        {
                            AppLogger.LogError("Check single daily schedules audio..!", e);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                AppLogger.LogError("Check daily schedules audio..!", e);
            }
        }

        private void EatAudioFiles()
        {
            var path = Path.Combine(_hostingEnvironment.ContentRootPath);

            string[] files = Directory.GetFiles(path + "\\AudioFiles");

            foreach(string file in files)
            {
                string filename = Path.GetFileNameWithoutExtension(file);

                var audio = _audiosRepository.GetAudioFileSystemName(filename);

                if(audio != null)
                {
                    audio.Bytes = File.ReadAllBytes(file);
                    audio.UpdateDate = DateTime.Now;

                    _audiosRepository.UpdateAudioFile(audio);
                }

                File.Delete(file);
            }
        }

        //--------------------------------------------------------------------------------------------------------------------//
        #endregion

        public void CreateAudioFile(string name, string prefix, int audioTypeID, int organizationID)
        {
            string filename = AppOps.CreateFileName(name);
            if (_audiosRepository.GetAudioFileSystemName(string.Format("{0}_{1}",prefix,filename)) == null)
            {
                _audiosRepository.CreateAudioFile(new Audio()
                {
                    OrganizationID = organizationID,
                    SystemName = string.Format("{0}_{1}", prefix, filename),
                    AudioTypeID = 2,
                    SpeakOutLtu = name,
                    SpeakOutEn = name,
                    Active = true,
                    Bytes = null,
                    UpdateDate = DateTime.Now
                });
            }
        }

        private class TransportTripDataComparer : IEqualityComparer<TransportTrip>
        {
            #region IEqualityComparer<object> Members

            bool IEqualityComparer<TransportTrip>.Equals(TransportTrip x, TransportTrip y)
            {
                return (((x as dynamic).ClientSystemID == (y as dynamic).ClientSystemID)
                    && ((x as dynamic).RouteName == (y as dynamic).RouteName)
                    && ((x as dynamic).StartDate == (y as dynamic).StartDate)
                    && ((x as dynamic).EndDate == (y as dynamic).EndDate));
            }

            int IEqualityComparer<TransportTrip>.GetHashCode(TransportTrip obj)
            {
                int TransportIdHash = obj.ClientSystemID.GetHashCode();
                int RouteNameHash = obj.RouteName == null ? 0 : obj.RouteName.GetHashCode();
                int StartHash = obj.StartDate.GetHashCode();
                int EndHash = obj.EndDate.GetHashCode();

                return TransportIdHash ^ RouteNameHash ^ StartHash ^ EndHash;
            }

            #endregion
        }
    }
}
