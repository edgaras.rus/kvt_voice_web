﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.SensorDto
{
    public class SensorSetting
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public int Type { get; set; }
    }
}
