﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Infrastructure.AppValidations
{
    public static class ModelDataValidation
    {
        public static ValidationResult GetErrors(this ModelStateDictionary modelState, bool includeListItems = false)
        {
            var result = (
                            from key in modelState.Keys
                            from err in modelState[key].Errors
                                        .GroupBy(z => z)
                                        .Where(z => z.Count() > 0 && (z.Key.ErrorMessage != null))
                                        .Select(z => z.Key.ErrorMessage)
                            where (includeListItems || (!includeListItems && key.IndexOf('.') == -1))
                                  && err.IndexOf("'_empty'") < 0
                            select new PropertyError
                            {
                                ColumnName = char.ToLower(key[0]) + key.Substring(1),
                                ErrorMessage = err
                            }
                          ).ToList();

            return new ValidationResult(result ?? new List<PropertyError>());
        }
    }
}
