﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Models.Transports;

namespace web_app_voice.Repositories.Interfaces
{
    public interface ISchedulesRepository
    {
        DailySchedule LoadDailyScheduleByOrganizationAndDeclare(int organizationID, DateTime declareDate);
        List<TransportTrip> TransportTripsBySchedule(int dailyScheduleID);
        void SaveDailySchedule(DailySchedule dailySchedule);
        List<DailySchedule> LoadDailySchedulesByDeclare(DateTime declareDate);

        DailySchedule LoadLastOrganizationDailySchedule(int organizationID);

        List<Station> LoadStations();

        void SaveStations(List<Station> stations);

        void UpdateStations(List<Station> stations);

    }
}
