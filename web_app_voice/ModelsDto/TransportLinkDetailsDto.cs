﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto
{
    public class TransportLinkDetailsDto
    {
        private const string EmptyData = "No Data";

        public int TransportTypeID { get; set; }

        [DisplayName("Route NO.")]
        public string RouteName { get; set; }


        [DisplayName("Direction")]
        public string DirectionName { get; set; }


        [DisplayName("Start Time")]
        public string FromDate { get; set; }


        [DisplayName("End Time")]
        public string ToDate { get; set; }


        [DisplayName("Play List")]
        public string PlayListLong { get; set; }


        [DisplayName("Play List(short)")]
        public string PlayListShort { get; set; }

        public TransportLinkDetailsDto()
        {
            this.TransportTypeID = 1;
            this.RouteName = EmptyData;
            this.DirectionName = EmptyData;
            this.FromDate = EmptyData;
            this.ToDate = EmptyData;
            this.PlayListLong = EmptyData;
            this.PlayListShort = EmptyData;
        }
    }
}
