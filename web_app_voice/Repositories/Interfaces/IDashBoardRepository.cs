﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.ModelsDto;

namespace web_app_voice.Repositories.Interfaces
{
    public interface IDashBoardRepository
    {
        List<DashBoardDto> LoadDashBoard(int? organizationID);
    }
}
