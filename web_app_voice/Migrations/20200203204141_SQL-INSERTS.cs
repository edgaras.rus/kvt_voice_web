﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace web_app_voice.Migrations
{
    public partial class SQLINSERTS : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO audios.\"AudioTypes\"(\"Name\")VALUES('System'),('Transport'),('Station');");

            migrationBuilder.Sql("INSERT INTO tags.\"TagLinkTypes\"(\"Name\")VALUES('Transport'),('Stations');");

            migrationBuilder.Sql("INSERT INTO tags.\"TagTypes\"(\"Name\")VALUES('Bus'),('Troley'),('MiniBus'),('Station');");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
