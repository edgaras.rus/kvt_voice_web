﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using web_app_voice.Models;
using web_app_voice.ModelsDto.SensorDto;
using web_app_voice.ModelsDto.TagsDto;
using web_app_voice.Repositories.Interfaces;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace web_app_voice.Controllers
{
    [Authorize]
    public class TagsController : Controller
    {

        private readonly ITagsRepository _tagsRepository;
        private readonly UserManager<User> _userManager;


        public TagsController(ITagsRepository tagsRepository, UserManager<User> userManager)
        {
            _tagsRepository = tagsRepository;
            _userManager = userManager;
        }


        // GET: /<controller>/
        public IActionResult Index()
        {

            var filter = new TagsFilterDto();
            filter.SetDefaultFilter();

            return View(new TagListDto
            {
                tagList = _tagsRepository.LoadTagListDto(filter, GetUser().OrganizationID),
                filter = filter,
                filterString = JsonConvert.SerializeObject(filter)

            });
        }

        public IActionResult GetPartialTagList(TagsFilterDto selector)
        {

            try
            {
                TagListDto result = new TagListDto
                {
                    tagList = _tagsRepository.LoadTagListDto(selector, GetUser().OrganizationID),
                    filter = selector,
                    filterString = JsonConvert.SerializeObject(selector)
                };
                return PartialView("~/Views/Tags/TagsList.cshtml", result);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public IActionResult GetTagPreview(int id)
        {
            DateTime date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            var taginfo = _tagsRepository.LoadTagDetailDto(id, date.AddDays(-30), date);
            return View("~/Views/Tags/TagPreview.cshtml", taginfo);
        }


        public IActionResult GetChartData(DateTime start, DateTime end, int id)
        {
            return new JsonResult(_tagsRepository.LoadTagDetailDto(id, start, end).TagChartDto);
        }


        [AllowAnonymous]
        [HttpPost]
        public IActionResult SaveTags(string parameters)
        {
            Sensor sensor = JsonConvert.DeserializeObject<Sensor>(parameters);
            try
            {
                _tagsRepository.SaveSensorData(sensor.Tags, sensor.SensorId.ToUpper());
            }
            catch (Exception e)
            {
                return new JsonResult("NOK");
            }

            return new JsonResult("OK");
        }

        private User GetUser()
        {
            return _userManager.FindByIdAsync(User.FindFirstValue(ClaimTypes.NameIdentifier)).Result;
        }

    }
}
