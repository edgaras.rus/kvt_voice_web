﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace web_app_voice.Extensions
{
    public static class LinqExtensions
    {
        public static IQueryable<T> SetSkip<T>(this IQueryable<T> source, int currentPage, int rowsPerPage)
        {
            return source.Skip((currentPage - 1) * rowsPerPage);
        }

        public static IQueryable<T> TakePage<T>(this IQueryable<T> source, int currentPage, int rowsPerPage)
        {
            return source.SetSkip(currentPage, rowsPerPage).Take(rowsPerPage);
        }

        public static List<T> GetList<T>(this IQueryable<T> source, int currentPage, int rowsPerPage)
        {
            return source.TakePage(currentPage, rowsPerPage).ToList();
        }

        public enum Order
        {
            Asc,
            Desc
        }

        public static IQueryable<T> OrderByDynamic<T>(this IQueryable<T> query, string orderByMember, Order direction)
        {
            var queryElementTypeParam = Expression.Parameter(typeof(T));

            var memberAccess = Expression.PropertyOrField(queryElementTypeParam, orderByMember);

            var keySelector = Expression.Lambda(memberAccess, queryElementTypeParam);

            var orderBy = Expression.Call(
                typeof(Queryable),
                direction == Order.Asc ? "OrderBy" : "OrderByDescending",
                new Type[] { typeof(T), memberAccess.Type },
                query.Expression,
                Expression.Quote(keySelector));

            return query.Provider.CreateQuery<T>(orderBy);
        }
    }
}
