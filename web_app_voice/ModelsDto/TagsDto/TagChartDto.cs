﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.TagsDto
{

    public class TagChartDto
    {

        public List<TagChartItemDto> ChartData { get; set; }

        public double MinRssi { get; set; }

        public double MaxRssi { get; set; }

        public double AvgRssi { get; set; }

        public TagChartDto()
        {
            this.ChartData = new List<TagChartItemDto>();

            this.MinRssi = 0;

            this.MaxRssi = 0;

            this.AvgRssi = 0;

        }
    }
}
