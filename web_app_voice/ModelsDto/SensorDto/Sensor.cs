﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.SensorDto
{
    public class Sensor
    {
        public string SensorId { get; set; }

        public List<SensorTag> Tags { get; set; }

        public List<SensorEvent> Events { get; set; }

        public Sensor(string SensorId)
        {
            this.SensorId = SensorId;
            this.Tags = new List<SensorTag>();
            this.Events = new List<SensorEvent>();
        }
    }
}
