﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Transports
{
    [Table("DailySchedules", Schema = "transports")]
    public class DailySchedule
    {
        [Key]
        public int DailyTransportTripID { get; set; }

        public int OrganizationID { get; set; }

        public DateTime DeclareDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public int Added { get; set; }

        public int Eddited { get; set; }

        public int Removed { get; set; }

        public virtual List<TransportTrip> TransportTrips { get; set; }

        public DailySchedule()
        {
            this.TransportTrips = new List<TransportTrip>();
        }
    }
}
