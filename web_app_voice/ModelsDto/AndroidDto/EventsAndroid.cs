﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.AndroidDto
{
    public class EventsAndroid
    {
        public string EventDate { get; set; }

        public int EventType { get; set; } //1 Info /2 warrning /3 Error

        public string Message { get; set; }

        public string StackTrance { get; set; }
    }
}
