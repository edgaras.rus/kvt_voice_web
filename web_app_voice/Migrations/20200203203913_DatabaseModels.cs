﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace web_app_voice.Migrations
{
    public partial class DatabaseModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "audios");

            migrationBuilder.EnsureSchema(
                name: "organizations");

            migrationBuilder.EnsureSchema(
                name: "tags");

            migrationBuilder.EnsureSchema(
                name: "transports");

            migrationBuilder.CreateTable(
                name: "Audios",
                schema: "audios",
                columns: table => new
                {
                    AudioID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    OrganizationID = table.Column<int>(nullable: false),
                    SystemName = table.Column<string>(nullable: false),
                    AudioTypeID = table.Column<int>(nullable: false),
                    SpeakOutLtu = table.Column<string>(nullable: true),
                    SpeakOutEn = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Bytes = table.Column<byte[]>(nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Audios", x => x.AudioID);
                });

            migrationBuilder.CreateTable(
                name: "AudioTypes",
                schema: "audios",
                columns: table => new
                {
                    AudioTypeID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AudioTypes", x => x.AudioTypeID);
                });

            migrationBuilder.CreateTable(
                name: "Organizations",
                schema: "organizations",
                columns: table => new
                {
                    OrganizationID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    PhoneNo = table.Column<string>(nullable: true),
                    Enable = table.Column<bool>(nullable: false),
                    GUID = table.Column<string>(nullable: false),
                    StationsDataUrl = table.Column<string>(nullable: false),
                    TripsDataUrl = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Organizations", x => x.OrganizationID);
                });

            migrationBuilder.CreateTable(
                name: "TagLinks",
                schema: "tags",
                columns: table => new
                {
                    TagLinkID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    OrganizationID = table.Column<int>(nullable: false),
                    TagLinkTypeID = table.Column<int>(nullable: false),
                    TagID = table.Column<int>(nullable: false),
                    ClientSystemID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TagLinks", x => x.TagLinkID);
                });

            migrationBuilder.CreateTable(
                name: "TagLinkTypes",
                schema: "tags",
                columns: table => new
                {
                    TagLinkTypeID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TagLinkTypes", x => x.TagLinkTypeID);
                });

            migrationBuilder.CreateTable(
                name: "TagTypes",
                schema: "tags",
                columns: table => new
                {
                    TagTypeID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TagTypes", x => x.TagTypeID);
                });

            migrationBuilder.CreateTable(
                name: "DailySchedules",
                schema: "transports",
                columns: table => new
                {
                    DailyTransportTripID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    OrganizationID = table.Column<int>(nullable: false),
                    DeclareDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    Added = table.Column<int>(nullable: false),
                    Eddited = table.Column<int>(nullable: false),
                    Removed = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DailySchedules", x => x.DailyTransportTripID);
                });

            migrationBuilder.CreateTable(
                name: "Stations",
                schema: "transports",
                columns: table => new
                {
                    StationID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    OrganizationID = table.Column<int>(nullable: false),
                    ClientSystemID = table.Column<string>(nullable: false),
                    StationName = table.Column<string>(nullable: false),
                    Latitude = table.Column<double>(nullable: true),
                    Longitude = table.Column<double>(nullable: true),
                    PlayList = table.Column<string>(nullable: true),
                    ShortPlayList = table.Column<string>(nullable: true),
                    ContentLithunian = table.Column<string>(nullable: true),
                    ShortContentLithunian = table.Column<string>(nullable: true),
                    ContentEnglish = table.Column<string>(nullable: true),
                    ShortContentEnglish = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stations", x => x.StationID);
                });

            migrationBuilder.CreateTable(
                name: "OrganizationScanners",
                schema: "organizations",
                columns: table => new
                {
                    OrganizationScannerID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    GUID = table.Column<string>(nullable: false),
                    OrganizationID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    LastComunicationDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrganizationScanners", x => x.OrganizationScannerID);
                    table.ForeignKey(
                        name: "FK_OrganizationScanners_Organizations_OrganizationID",
                        column: x => x.OrganizationID,
                        principalSchema: "organizations",
                        principalTable: "Organizations",
                        principalColumn: "OrganizationID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                schema: "tags",
                columns: table => new
                {
                    TagID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TagMAC = table.Column<string>(nullable: true),
                    TagNamespace = table.Column<string>(nullable: false),
                    TagInstance = table.Column<string>(nullable: false),
                    TagUrl = table.Column<string>(nullable: true),
                    TagTypeID = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    LastSeenDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.TagID);
                    table.ForeignKey(
                        name: "FK_Tags_TagTypes_TagTypeID",
                        column: x => x.TagTypeID,
                        principalSchema: "tags",
                        principalTable: "TagTypes",
                        principalColumn: "TagTypeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TransportTrips",
                schema: "transports",
                columns: table => new
                {
                    TransportTripID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DailyTransportTripID = table.Column<int>(nullable: false),
                    ClientSystemID = table.Column<string>(nullable: false),
                    RouteName = table.Column<string>(nullable: true),
                    DirectionID = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    PlayList = table.Column<string>(nullable: true),
                    ShortPlayList = table.Column<string>(nullable: true),
                    ContentLithuania = table.Column<string>(nullable: true),
                    ShortContentLithuania = table.Column<string>(nullable: true),
                    ContentEnglish = table.Column<string>(nullable: true),
                    ShortContentEnglish = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransportTrips", x => x.TransportTripID);
                    table.ForeignKey(
                        name: "FK_TransportTrips_DailySchedules_DailyTransportTripID",
                        column: x => x.DailyTransportTripID,
                        principalSchema: "transports",
                        principalTable: "DailySchedules",
                        principalColumn: "DailyTransportTripID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TagDetails",
                schema: "tags",
                columns: table => new
                {
                    TagDetailID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TagID = table.Column<int>(nullable: false),
                    OrganizationScannerID = table.Column<int>(nullable: true),
                    RSSI = table.Column<int>(nullable: false),
                    RecordDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TagDetails", x => x.TagDetailID);
                    table.ForeignKey(
                        name: "FK_TagDetails_Tags_TagID",
                        column: x => x.TagID,
                        principalSchema: "tags",
                        principalTable: "Tags",
                        principalColumn: "TagID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrganizationScanners_OrganizationID",
                schema: "organizations",
                table: "OrganizationScanners",
                column: "OrganizationID");

            migrationBuilder.CreateIndex(
                name: "IX_TagDetails_TagID",
                schema: "tags",
                table: "TagDetails",
                column: "TagID");

            migrationBuilder.CreateIndex(
                name: "IX_Tags_TagTypeID",
                schema: "tags",
                table: "Tags",
                column: "TagTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_TransportTrips_DailyTransportTripID",
                schema: "transports",
                table: "TransportTrips",
                column: "DailyTransportTripID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Audios",
                schema: "audios");

            migrationBuilder.DropTable(
                name: "AudioTypes",
                schema: "audios");

            migrationBuilder.DropTable(
                name: "OrganizationScanners",
                schema: "organizations");

            migrationBuilder.DropTable(
                name: "TagDetails",
                schema: "tags");

            migrationBuilder.DropTable(
                name: "TagLinks",
                schema: "tags");

            migrationBuilder.DropTable(
                name: "TagLinkTypes",
                schema: "tags");

            migrationBuilder.DropTable(
                name: "Stations",
                schema: "transports");

            migrationBuilder.DropTable(
                name: "TransportTrips",
                schema: "transports");

            migrationBuilder.DropTable(
                name: "Organizations",
                schema: "organizations");

            migrationBuilder.DropTable(
                name: "Tags",
                schema: "tags");

            migrationBuilder.DropTable(
                name: "DailySchedules",
                schema: "transports");

            migrationBuilder.DropTable(
                name: "TagTypes",
                schema: "tags");
        }
    }
}
