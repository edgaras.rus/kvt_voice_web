﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Data;
using web_app_voice.Models.System;
using web_app_voice.Repositories.Interfaces;

namespace web_app_voice.Repositories
{
    public class RepetitiveTaskRepository : IRepetitiveTaskRepository
    {

        private readonly ApplicationDbContext _context;

        public RepetitiveTaskRepository(ApplicationDbContext applicationDbContext)
        {
            _context = applicationDbContext;
        }

        public List<RepetitiveTask> LoadRepetitiveTasks(bool inactive = false)
        {
            if(inactive == true)
                return _context.RepetitiveTasks.ToList();

            return _context.RepetitiveTasks.Where(x => x.Enabled == true).ToList();
        }

        public void UpdateRepetitiveTask(RepetitiveTask repetitiveTask)
        {
            _context.RepetitiveTasks.Update(repetitiveTask);
            _context.SaveChanges();
        }


    }
}
