﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace web_app_voice.Migrations
{
    public partial class UserOrganizationID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StationDirection",
                schema: "transports",
                table: "Stations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TagLinks_OrganizationID",
                schema: "tags",
                table: "TagLinks",
                column: "OrganizationID");

            migrationBuilder.CreateIndex(
                name: "IX_TagLinks_TagLinkTypeID",
                schema: "tags",
                table: "TagLinks",
                column: "TagLinkTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Audios_AudioTypeID",
                schema: "audios",
                table: "Audios",
                column: "AudioTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Audios_OrganizationID",
                schema: "audios",
                table: "Audios",
                column: "OrganizationID");

            migrationBuilder.AddForeignKey(
                name: "FK_Audios_AudioTypes_AudioTypeID",
                schema: "audios",
                table: "Audios",
                column: "AudioTypeID",
                principalSchema: "audios",
                principalTable: "AudioTypes",
                principalColumn: "AudioTypeID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Audios_Organizations_OrganizationID",
                schema: "audios",
                table: "Audios",
                column: "OrganizationID",
                principalSchema: "organizations",
                principalTable: "Organizations",
                principalColumn: "OrganizationID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TagLinks_Organizations_OrganizationID",
                schema: "tags",
                table: "TagLinks",
                column: "OrganizationID",
                principalSchema: "organizations",
                principalTable: "Organizations",
                principalColumn: "OrganizationID",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Audios_AudioTypes_AudioTypeID",
                schema: "audios",
                table: "Audios");

            migrationBuilder.DropForeignKey(
                name: "FK_Audios_Organizations_OrganizationID",
                schema: "audios",
                table: "Audios");

            migrationBuilder.DropForeignKey(
                name: "FK_TagLinks_Organizations_OrganizationID",
                schema: "tags",
                table: "TagLinks");

            migrationBuilder.DropForeignKey(
                name: "FK_TagLinks_TagLinkTypes_TagLinkTypeID",
                schema: "tags",
                table: "TagLinks");

            migrationBuilder.DropIndex(
                name: "IX_TagLinks_OrganizationID",
                schema: "tags",
                table: "TagLinks");

            migrationBuilder.DropIndex(
                name: "IX_TagLinks_TagLinkTypeID",
                schema: "tags",
                table: "TagLinks");

            migrationBuilder.DropIndex(
                name: "IX_Audios_AudioTypeID",
                schema: "audios",
                table: "Audios");

            migrationBuilder.DropIndex(
                name: "IX_Audios_OrganizationID",
                schema: "audios",
                table: "Audios");

            migrationBuilder.DropColumn(
                name: "StationDirection",
                schema: "transports",
                table: "Stations");
        }
    }
}
