﻿"use strict";

var login = function (options) {

    var vars = {
        laddaBtnSave: null
    };

    this.construct = function (options) {
        $.extend(vars, options);

        initLogin();
    }


    var initLogin = function () {

        vars.laddaBtnSave = $('#btnLogin').ladda();

        $("#btnLogin").click(function () {
            var loginDataDto = {
                LoginEmail: $("#loginEmail").val(),
                LoginPassword: $("#loginPassword").val(),
                ReturnUrl: $("#ReturnUrl").val()
            }

            var json = JSON.stringify(loginDataDto);
            $.ajax({
                type: "POST",
                url: options.loginAction,
                data: loginDataDto,
                beforeSend: function () {
                    //vars.laddaBtnSave.ladda('start');
                    $('#loginEmailError, #loginPasswordError, #loginError').hide();
                    $('#loginEmail, #loginPassword').removeClass('input-validation-error');
                },
                success: function (data) {
                    if (data.hasError) {
                        $.each(data.errors, function (index, value) {
                            if (value.columnName) {
                                $('#' + value.columnName).addClass('input-validation-error');
                                $('#' + value.columnName + 'Error').html(value.errorMessage);
                                $('#' + value.columnName + 'Error').show();
                            } else {
                                $('#loginError').html(value.errorMessage);
                                $('#loginError').show();
                            }
                        });
                    } else {
                        location.href = data;
                    }
                },
                complete: function () {
                    vars.laddaBtnSave.ladda('stop');
                }
            });
        });
    }

    this.construct(options);
}