﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.AndroidDto
{
    public class RouteAndroid
    {
        public string TransportId { get; set; }

        public string TagNo { get; set; }

        public int OrganizationId { get; set; }

        public string Route { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string RouteId { get; set; }

        public string RouteName { get; set; }

        public string PlayList { get; set; }

        public string ShortPlayList { get; set; }

        public string ContentLithuania { get; set; }

        public string ShortContentLithuania { get; set; }

        public string ContentEnglish { get; set; }

        public string ShortContentEnglish { get; set; }

        public RouteAndroid()
        {
            this.TagNo = string.Empty;
        }
    }
}
