﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Organizations
{
    [Table("Organizations", Schema = "organizations")]
    public class Organization
    {
        [Key]
        public int OrganizationID { get; set; }

        [Required]
        public string Name { get; set; }

        [EmailAddress]
        public string Address { get; set; }

        [Phone]
        public string PhoneNo { get; set; }

        [DefaultValue(true)]
        public bool Enable { get; set; }

        [Required]
        public string GUID { get; set; }

        [Required]
        public string StationsDataUrl { get; set; }

        [Required]
        public string TripsDataUrl { get; set; }
        
        public string OrganizationLogo { get; set; }


        public virtual List<OrganizationScanner> OrganizationScanners { get; set; }

        public Organization()
        {
            this.OrganizationScanners = new List<OrganizationScanner>();
        }
    }
}
