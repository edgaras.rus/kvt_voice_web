﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto
{
    public class DashBoardScannerDto
    {
        public string Name { get; set; }

        public DateTime LastUpdateDate { get; set; }
    }
}
