﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.AndroidDto
{
    public class VoiceData
    {
        public List<RouteAndroid> RoutesDataList { get; set; }

        public List<StationAndroid> StationsDataList { get; set; }

        public List<AudioAndroid> AudioFilesInfoList { get; set; }

        public string AudioFilesGenerateDate { get; set; }

        public string RoutesGenerateDate { get; set; }

        public string StationsGanerateDate { get; set; }

    }
}
