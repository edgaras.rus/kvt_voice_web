﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.SensorDto
{
    public class SensorTag
    {
        public DateTime EventDate { get; set; }

        public string Mac { get; set; }

        public int Rssi { get; set; }

        public string Instance { get; set; }

        public string Namespace { get; set; }

        public string Url { get; set; }
    }
}
