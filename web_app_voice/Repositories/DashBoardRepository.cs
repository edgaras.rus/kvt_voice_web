﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Data;
using web_app_voice.ModelsDto;
using web_app_voice.Repositories.Interfaces;

namespace web_app_voice.Repositories
{
    public class DashBoardRepository : IDashBoardRepository
    {
        private readonly ApplicationDbContext _context;

        public DashBoardRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<DashBoardDto> LoadDashBoard(int? organizationID)
        {
            List<DashBoardDto> result = new List<DashBoardDto>();

            try
            {
                if (organizationID.HasValue)
                {
                    var organizations = _context.Organizations.Where(x => x.OrganizationID == organizationID.Value).Include(o => o.OrganizationScanners).ToList();

                    foreach (var organization in organizations)
                    {
                        DashBoardDto dashBoardDto = new DashBoardDto()
                        {
                            OrganizationLogo = organization.OrganizationLogo,
                            OrganizationName = organization.Name
                        };

                        foreach (var organizationScaner in organization.OrganizationScanners)
                        {
                            dashBoardDto.OrganizationScaners.Add(new DashBoardScannerDto()
                            {
                                Name = organizationScaner.Name,
                                LastUpdateDate = organizationScaner.LastComunicationDate
                            });
                        }

                        dashBoardDto.LastStationsUpdateList = _context.RepetitiveTasks.Where(x => x.RepetitiveTaskID == 2).FirstOrDefault().LastExecuteTime.Value;

                        dashBoardDto.LastTransportRoutesUpdateTime = _context.DailySchedules
                            .Where(x => x.OrganizationID == organization.OrganizationID).OrderByDescending(x => x.UpdateDate).FirstOrDefault().UpdateDate;

                        result.Add(dashBoardDto);
                    }
                }
                else
                {
                    var organizations = _context.Organizations.Include(o => o.OrganizationScanners).ToList();

                    foreach(var organization in organizations)
                    {
                        try
                        {
                            DashBoardDto dashBoardDto = new DashBoardDto()
                            {
                                OrganizationLogo = organization.OrganizationLogo,
                                OrganizationName = organization.Name
                            };

                            foreach (var organizationScaner in organization.OrganizationScanners)
                            {
                                dashBoardDto.OrganizationScaners.Add(new DashBoardScannerDto()
                                {
                                    Name = organizationScaner.Name,
                                    LastUpdateDate = organizationScaner.LastComunicationDate
                                });
                            }

                            dashBoardDto.LastStationsUpdateList = _context.RepetitiveTasks.Where(x => x.RepetitiveTaskID == 2).FirstOrDefault().LastExecuteTime.Value;

                            dashBoardDto.LastTransportRoutesUpdateTime = _context.DailySchedules
                                .Where(x => x.OrganizationID == organization.OrganizationID).OrderByDescending(x => x.UpdateDate).FirstOrDefault().UpdateDate;

                            result.Add(dashBoardDto);
                        }catch(Exception e) { }
                    }
                }
            }
            catch(Exception e)
            {

            }

            return result;

        }
    }
}
