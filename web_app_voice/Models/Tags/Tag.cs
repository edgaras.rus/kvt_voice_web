﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Tags
{
    [Table("Tags", Schema = "tags")]
    public class Tag
    {
        [Key]
        public int TagID { get; set; }

        public string TagMAC { get; set; }

        [Required]
        public string TagNamespace { get; set; }

        [Required]
        public string TagInstance { get; set; }

        public string TagUrl { get; set; }

        [Required]
        public int TagTypeID { get; set; }

        [DefaultValue(true)]
        public bool IsActive { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime LastSeenDate { get; set; }

        [ForeignKey("TagTypeID")]
        public virtual TagType TagType { get; set; }

        public virtual List<TagDetail> TagDetails { get; set; }

        public Tag()
        {
            this.CreateDate = DateTime.Now;
            this.LastSeenDate = DateTime.Now;
        }
    }
}
