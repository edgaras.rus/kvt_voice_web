﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto
{
    public class LoginDataDto
    {
        [Required(ErrorMessage = "Email is required")]
        public string LoginEmail { get; set; }

        [Required(ErrorMessage = "Password is required")]
        public string LoginPassword { get; set; }

        public string ReturnUrl { get; set; }
    }
}
