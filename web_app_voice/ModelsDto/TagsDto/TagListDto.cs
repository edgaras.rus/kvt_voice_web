﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.TagsDto
{
    public class TagListDto
    {
        public List<TagDto> tagList { get; set; }

        public TagsFilterDto filter { get; set; }

        public string filterString { get; set; }
    }
}
