﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Enums
{
    public enum SortTypes
    {
        [Description("Ascending")]
        Asc = 10,
        [Description("Descending")]
        Desc = 20,
    }
}
