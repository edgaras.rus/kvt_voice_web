﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.AndroidDto
{
    public class DataAndroid
    {
        public string DeviceKey { get; set; }

        public string AndroidAudioUpdateDate { get; set; }

        public string AndroidRoutesUpdateDate { get; set; }

        public string AndroidStationsUpdateDate { get; set; }

        public List<StationBeaconsAndroid> StationBeaconsAndroidList { get; set; }

        public List<EventsAndroid> EventsAndroidList { get; set; }

        public DataAndroid()
        {
            this.DeviceKey = String.Empty;
            this.AndroidAudioUpdateDate = DateTime.MinValue.ToString();
            this.AndroidRoutesUpdateDate = DateTime.MinValue.ToString();
            this.AndroidStationsUpdateDate = DateTime.MinValue.ToString();
            this.StationBeaconsAndroidList = new List<StationBeaconsAndroid>();
            this.EventsAndroidList = new List<EventsAndroid>();
        }
    }
}
