﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace web_app_voice.Migrations
{
    public partial class LastPlanedDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LastPlanedDate",
                schema: "tags",
                table: "TagLinks",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastPlanedDate",
                schema: "tags",
                table: "TagLinks");
        }
    }
}
