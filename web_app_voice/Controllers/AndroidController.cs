﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_app_voice.Models.Audio;
using web_app_voice.ModelsDto.AndroidDto;
using web_app_voice.ModelsDto.SensorDto;
using web_app_voice.Repositories.Interfaces;

namespace web_app_voice.Controllers
{
    public class AndroidController : Controller
    {

        private readonly ITagsRepository _tagsRepository;
        private readonly IAudiosRepository _audiosRepository;

        public AndroidController(ITagsRepository tagsRepository, IAudiosRepository audiosRepository)
        {
            _tagsRepository = tagsRepository;
            _audiosRepository = audiosRepository;
        }


        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult GetVoiceData(string json)
        {
            try
            {
                DataAndroid data = new DataAndroid();

                if (json != null)
                    data = JsonConvert.DeserializeObject<DataAndroid>(json);

                VoiceData result = new VoiceData();


                List<SensorTag> DataFromAndroidDevice = new List<SensorTag>();
                foreach (StationBeaconsAndroid item in data.StationBeaconsAndroidList)
                {
                    SensorTag st = new SensorTag
                    {
                        EventDate = DateTime.Parse(item.EventDate),
                        Mac = item.Mac,
                        Rssi = Int32.Parse(item.Rssi),
                        Instance = item.Instance,
                        Namespace = item.NameSpace,
                    };
                    DataFromAndroidDevice.Add(st);
                }
                if (DataFromAndroidDevice != null && DataFromAndroidDevice.Count > 0)
                    _tagsRepository.SaveSensorData(DataFromAndroidDevice);

                /*
                List<EventData> EventsFromAndroidDevice = new List<EventData>();
                foreach (EventsAndroid item in data.EventsAndroidList)
                {
                    EventsFromAndroidDevice.Add(new EventData
                    {
                        EventTime = DateTime.Parse(item.EventDate),
                        EventType = item.EventType,
                        UserRoleType = (int)EventUserType.User,
                        Description = String.Format("Message : {0} StackTrance: {1}", item.Message, item.StackTrance),
                        UserName = data.DeviceKey,
                    });
                }
                if (EventsFromAndroidDevice != null && EventsFromAndroidDevice.Count > 0)
                    eventRepository.SaveEvent(EventsFromAndroidDevice);

                */


                DateTime AndroidRoutesUpdateDate = DateTime.MinValue;
                DateTime.TryParse(data.AndroidRoutesUpdateDate, out AndroidRoutesUpdateDate);
                result.RoutesDataList = _tagsRepository.GetRouteAndroidData(AndroidRoutesUpdateDate);
                result.RoutesGenerateDate = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");

                DateTime AndroidStationUpdateDate = DateTime.MinValue;
                DateTime.TryParse(data.AndroidStationsUpdateDate, out AndroidStationUpdateDate);
                result.StationsDataList = _tagsRepository.GetStationAndroidList(AndroidStationUpdateDate);
                result.StationsGanerateDate = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");


                DateTime AndroidAudiosUpdateDate = DateTime.MinValue;
                DateTime.TryParse(data.AndroidAudioUpdateDate, out AndroidAudiosUpdateDate);
                result.AudioFilesInfoList = _audiosRepository.GetAndroidAudioFilesList(AndroidAudiosUpdateDate);
                result.AudioFilesGenerateDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                return new JsonResult(JsonConvert.SerializeObject(result));
            }
            catch (Exception e)
            {

            }
            return null;

        }

        [HttpPost]
        public IActionResult PostArgumentTest(string value)
        {
            return new JsonResult(JsonConvert.SerializeObject(value));
        }

        ///Android/GetAudioFile?Id=1
        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetAudioFile(int audioID)
        {
            Audio audio = _audiosRepository.GetAudioFileById(audioID);
            if (audio == null && audio.Bytes != null)
            {
                return null;
            }
            return File(audio.Bytes, "audio/mpeg", String.Format("{0}.mp3", audio.SystemName));
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetAudioFileBySystemName(string systemName)
        {
            Audio audio = _audiosRepository.GetAudioFileSystemName(systemName);
            if (audio == null || audio.Bytes == null)
            {
                return null;
            }
            return File(audio.Bytes, "audio/mpeg", String.Format("{0}.mp3", audio.SystemName));
        }

    }
}
