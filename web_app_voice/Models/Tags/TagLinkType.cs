﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.Models.Tags
{
    [Table("TagLinkTypes", Schema = "tags")]
    public class TagLinkType
    {
        [Key]
        public int TagLinkTypeID { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
