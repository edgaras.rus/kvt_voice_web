﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto.AndroidDto
{
    public class StationAndroid
    {
        public string StationId { get; set; }

        public string TagNo { get; set; }

        public int OrganizationId { get; set; }

        public string StationName { get; set; }

        public string Direction { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string PlayList { get; set; }

        public string ContentLithuania { get; set; }

        public string ContentEnglish { get; set; }

        public StationAndroid()
        {
            this.TagNo = string.Empty;
        }
    }
}
