﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web_app_voice.ModelsDto
{
    public class TagLinkCreateEditDto
    {
        public int TagLinkID { get; set; }

        [Required]
        public int OrganizationID { get; set; }

        public string OrganizationName { get; set; }

        [Required]
        public int TagLinkTypeID { get; set; }

        public string TagLinkTypeName { get; set; }

        [Required]
        public int TagID { get; set; }

        [Required]
        public string ClientSystemID { get; set; }

        public List<SelectListItem> Organizations { get; set; }

        public List<SelectListItem> TagLinkTypes { get; set; }


        public TagLinkCreateEditDto()
        {
            Organizations = new List<SelectListItem>();
            TagLinkTypes = new List<SelectListItem>()
            {
                new SelectListItem
                {
                    Text = "System",
                    Value = "1"
                    
                },

                new SelectListItem
                {
                    Text ="Transport",
                    Value = "2"
                },

                 new SelectListItem
                {
                    Text ="Station",
                    Value = "3"
                },
            };
        }
    }
}
