﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace web_app_voice.Migrations
{
    public partial class Update20200220 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LastSeenDate",
                schema: "tags",
                table: "TagLinks",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastSeenDate",
                schema: "tags",
                table: "TagLinks");
        }
    }
}
