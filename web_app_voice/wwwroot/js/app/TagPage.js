﻿var tagList = function (param) {
    var tagGetList = {};
    var JSONData = $.parseJSON($("#FilterJson").val());

    if (JSONData !== null) {

        var _TransportValues = JSONData.TransportTypes;
        if (_TransportValues !== null && _TransportValues.length > 0) {
            for (var i = 0; i < _TransportValues.length; i++) {
                var value = _TransportValues[i];
                if (value === 10)
                    $("#Bus").prop('checked', true);
                if (value === 20)
                    $("#Trolley").prop('checked', true);
                if (value === 30)
                    $("#Station").prop('checked', true);
            }
        }

        var _StateValues = JSONData.StateValues;
        if (_StateValues !== null && _StateValues.length > 0) {
            for (var i = 0; i < _StateValues.length; i++) {
                var value = _StateValues[i];
                if (value === 10)
                    $("#Online").prop('checked', true);
                if (value === 20)
                    $("#Warrning").prop('checked', true);
                if (value === 30)
                    $("#Error").prop('checked', true);
            }
        }
    }

    tagGetList.updateWrapData = function (input) {
        var stateArray = [];
        var transportArray = [];
        var tagStatusArray = [];
        var searchText = "";
        var transportSortType = 0;
        var sortType = 10;


        $(".transportTypeEnum").map(function (idx, item) {
            if ($(item).iCheck('data')[0].checked) {
                var value = $(item).attr("stateenum");
                transportArray.push(value)
            }
        });

        $(".transportStateEnum").map(function (idx, item) {
            if ($(item).iCheck('data')[0].checked) {
                var value = $(item).attr("stateenum");
                stateArray.push(value)
            }
        });

        $(".TagStateEnum").map(function (idx, item) {
            if ($(item).iCheck('data')[0].checked) {
                var value = $(item).attr("stateenum");
                tagStatusArray.push(value)
            }
        });

        searchText = $('#transportSerchBox').val();
        if (searchText == null) {
            searchText = "";
        }

        transportSortType = $("#transportSorter").val();
        sortType = $("#sorter").val();

        $.ajax({
            url: param.getPartialListUrl,
            type: "POST",
            dataType: "text",
            data: {
                selector: {
                    TransportTypes: transportArray,
                    StateValues: stateArray,
                    TagStatus: tagStatusArray,
                    Search: searchText,
                    TransportSortType: transportSortType,
                    SortType: sortType,
                },
               
            },
            success: function (data) {
                $("#tagDataList").html(data);
            },
            error: function (e) {
                $("#tagDataList").html();
            }
        });
    };

    tagGetList.init = function () {
        $(".i-checks").iCheck({
            checkboxClass: "icheckbox_square-green",
            radioClass: "iradio_square-green"
        });
    };

    $(".transportTypeEnum, .transportStateEnum, .TagStateEnum").on("ifChanged", function () {
        tagGetList.updateWrapData();
    });

    $('#transportSerchBox').on('input', function () {
        tagGetList.updateWrapData();
    });

    $('select').on('change', function () {
        tagGetList.updateWrapData();
    })

    $("#clearFilters").click(function (e) {

        $("#Bus").prop('checked', true).iCheck('update');
        $("#Trolley").prop('checked', true).iCheck('update');
        $("#Station").prop('checked', true).iCheck('update');

        $("#Online").prop('checked', true).iCheck('update');
        $("#Warrning").prop('checked', true).iCheck('update');
        $("#Error").prop('checked', true).iCheck('update');

        $("#Registered").prop('checked', true).iCheck('update');
        $("#Unregistered").prop('checked', false).iCheck('update');


        $('#transportSerchBox').val("");

        $('#transportSorter').val("0");
        $('#sorter').val("10");

        tagGetList.updateWrapData();
    });

  

    $("#add_new_tag").click(function (e) {
        $.ajax({
            url: '/Tags/OpenCreateModal',
            type: "POST",
            dataType: "text",
            data: {
                
            },
            error: function (data)
            { },
            success: function (data) {
                console.log(data)
                $("#new_tag").html(data);
            },
        });
    });

    tagGetList.init();
};

function showDetails(clickedElement) {
    var dataid = $(clickedElement).attr("data-id");
    showDetailsAjax(dataid);
}

function showDetailsAjax(activityAdID) {
    var link = '@Url.Action("GetTagPreview", "Tags", new { id = "-1"})'
    link = link.replace("-1", activityAdID);



    $.ajax({
        url: '/Tags/GetTagPreview',
        type: "POST",
        dataType: "text",
        data: {
            id: activityAdID,
        },
        error: function (data)
        { },
        success: function (data) {
            $("#detailsModal .modal-body").html(data);
            $('#detailsModal').modal('show');
        },
    });
}
